<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['public/(:any)'] = 'public/$1';
// $route['(:any)'] = 'Devede/view/$1';
$route['default_controller'] = 'Demarrage/accueil';
$route['resultatRecherche'] = 'Cours/findCours';

// Gestion des diplomes
$route['ajouterDiplome'] = 'Diplomes/creationDip';
$route['supprimerDiplome'] = 'Diplomes/deleteDip';
$route['listDiplome'] = 'Diplomes/allDiplome';
$route['modifierDiplome'] = 'Diplomes/modifierDiplome';

// Gestion des programmes
$route['creerProgramme'] = 'Programme/creationProg';
$route['supprimerProgramme'] = 'Programme/deleteProg';
$route['listProgramme'] = 'Programme/allProg';
$route['modifierProgramme'] = 'Programme/modifierProg';

// Gestion des cours
$route['creerCours'] = 'Cours/creationCours';
$route['supprimerCours'] = 'cours/deleteCours';
$route['listCours'] = 'Cours/allCours';
$route['modifierCours'] = 'Cours/modifierCours';

// Gestion des Universités
$route['creerUniv'] = 'Universites/creationUniv';
$route['supprimerUniv'] = 'Universites/deleteUniv';
$route['listUniv'] = 'Universites/allUniv';
$route['modifierUniv'] = 'Universites/modifierUniv';

// Financement et pages étudiantes :
$route['listeFinancement'] = "Financement/listeFinancement";
$route['detailsFinancement'] = "Financement/getAllDetails";


// Mobilité étudiantes :
$route['listeMobilite'] = "Mobilite/listeMobilite";
$route['detailsMobilite'] = "Mobilite/getAllMobiliteDetails";

// Contrat étudiants :
$route['listeContrat'] = "Contrat/listeContrat";
$route['detailsContrat']= "Contrat/getAllContratDetails";


// Coin recherches :
$route['coursParDiplome'] = 'Diplomes/coursParDiplome';
$route['progParUniv'] = 'Programme/progParUniv';
$route['diplomesParUniv'] = 'Diplomes/diplomesParUniv';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
