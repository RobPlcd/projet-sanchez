<h1>Modifier le programme <?= $modifier[0]['nomProgramme']; ?></h1>

<form action="<?php echo base_url();?>index.php/modifierProgramme" method ="post">
    <input type="hidden" value="<?= $modifier[0]['codeProgramme']; ?>" name="code">

      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du Programme :</label>
          <input type="text" class="form-control" id="nomProgramme" value="<?= $modifier[0]['nomProgramme']; ?>" name="nomProgramme" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Durée Echange :</label>
            <input type="text" class="form-control" id="dureeEchange" value="<?= $modifier[0]['dureeEchange']; ?>" name="dureeEchange" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Diplomes associées :</label>
            <select class="form-control" name="nomUn">
                <?php

                    foreach ($diplome as $value){
                        if($value['nomDiplome'] == $modifier[0]['nomUn'] ){
                            echo "<option value='". $value['codeDiplome'] ."' selected>". $value['nomDiplome'] ."</option>";
                        } else {
                            echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";  
                        }
                    }

                ?>
            </select>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Diplomes associés :</label>
            <select class="form-control" name="nomDeux">
                <?php

                    foreach ($diplome as $value){
                        if($value['nomDiplome'] == $modifier[0]['nomDeux'] ){
                            echo "<option value='". $value['codeDiplome'] ."' selected>". $value['nomDiplome'] ."</option>";
                        } else {
                            echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";  
                        }
                    }

                ?>
            </select>
      </div>

        <input type="submit" value="Modifier <?= $modifier[0]['nomProgramme']; ?>" class="btn btn-primary">
</form>
