<h1>Diplômes par universités</h1>

<form action="<?php echo base_url();?>index.php/diplomesParUniv" method ="post">
    <div class="form-group">
        <label for="formulaire" class="form-label">Pour l'université :</label>
        <select class="form-control" name="codeUniv">
            <?php
            if(isset($univVoulu)){
                foreach ($univ as $value){
                    if($value['codeU'] == $univVoulu){
                        echo "<option value='". $value['codeU'] ."' selected>". $value['nomU'] ."</option>";
                    } else {
                        echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";
                    }
                }
            }
            else {
                foreach ($univ as $value){
                    echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";
                }
            }
            ?>
        </select>
    </div>

    <input type="submit" value="Rechercher les diplômes associés" class="btn btn-primary">
</form>

<?php
if(isset($diplomes)){
    ?>
    <table class="table mt-5">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du diplôme</th>
            <th scope="col">Niveau du diplôme</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($diplomes as $value){
            echo '<tr>
                        <th scope="row">'. $value['codeDiplome'] .'</th>
                        <td>'. $value['nomDiplome'] .'</td>
                        <td>'. $value['niveauDiplome'] .'</td>
                        <td><form action="'. base_url() .'index.php/modifierDiplome" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeDiplome'] .'" name="codeDiplome"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
        }

        ?>
        </tbody>
    </table>
    <?php
}
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>
