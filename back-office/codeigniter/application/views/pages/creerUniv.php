<h1>Ajouter une université</h1>

<form action="<?php echo base_url();?>index.php/creerUniv" method ="post">
    <div class="form-group">
        <label for="nomUniv" class="form-label">Nom de l'Université:</label>
        <input type="text" class="form-control" placeholder="Paul Sabatier Toulouse III" name ="nomUniv" required>
    </div>
    <div class="form-group">
        <label for="villeUniv" class="form-label">Ville de l'Université :</label>
        <input type="text" class="form-control" placeholder="Toulouse" name="villeUniv" required>
    </div>
    <div class="form-group">
        <label for="paysUniv" class="form-label">Pays de l'Université :</label>
        <input type="text" class="form-control" placeholder="France" name="paysUniv" required>
    </div>
    <div class="form-group">
        <label for="webUniv" class="form-label">Site web :</label>
        <input type="url" class="form-control" placeholder="https://www.univ-tlse3.fr" name="webUniv" size="49" pattern="https://.*" required>
    </div>
    <input type="submit" value="Ajouter cette université" class="btn btn-primary">
</form>


<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>
