<h1>Supprimer un programme</h1>

<form action="<?php echo base_url();?>index.php/supprimerProgramme" method ="post">
    <div class="form-group">
        <label for="formulaire" class="form-label">Programme à supprimer :</label>
        <select class="form-control" name="nomProgramme">
            <?php

            foreach ($programme as $value){
                echo "<option value='". $value['codeProgramme'] ."'>". $value['nomProgramme'] ."</option>";
            }

            ?>
        </select>
        <small class="form-text text-muted">Cette opération est irréversible.</small>
    </div>

    <input type="submit" value="Supprimer" class="btn btn-danger">
</form>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>