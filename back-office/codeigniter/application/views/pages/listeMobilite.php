<h1>Liste des demandes de mobilités</h1>

<table class="table mt-5 table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Date de la demande</th>
            <th scope="col">Etat</th>
            <th scope="col">Etudiant demandeur</th>
            <th scope="col">Programme concerné</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
    	<?php

    		foreach ($mobilite as $value) {
    			echo "<tr>
    				<th scope='row'>". $value['codeDemandeM']  ."</th>
    				<td>". $value['dateDepotDemandeM'] ."</td>";

    			if($value['etatDemandeM'] == "attente"){
    				echo "<td><span class='badge badge-pill badge-info'>". $value['etatDemandeM'] ."</span></td>";
    			} else if($value['etatDemandeM'] == "acceptée"){
    				echo "<td><span class='badge badge-pill badge-success'>". $value['etatDemandeM'] ."</span></td>";
    			} else {
    				echo "<td><span class='badge badge-pill badge-danger'>". $value['etatDemandeM'] ."<span></td>";
    			}

    			echo" <td>". $value['nomEtudiant'] ." ". $value['prenomEtudiant'] ."</td>
                    <td>". $value['codeProgramme'] ."</td>
    				<td><form action='". base_url() ."index.php/detailsMobilite' method='post' class='mt-0 mb-0 pt-0 pb-0'><input name='codeDemandeM' type='hidden' value='". $value['codeDemandeM'] ."'><input type='submit' value='Détails' class='btn-sm btn-secondary'></form></td>
    			</tr>";
    		}

    	?>
    </tbody>
</table>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>