<h1>Programmes par universités</h1>

<form action="<?php echo base_url();?>index.php/progParUniv" method ="post">
    <div class="form-group">
        <label for="formulaire" class="form-label">Pour l'Université :</label>
        <select class="form-control" name="codeU">
            <?php
            if(isset($univVoulu)){
                foreach ($univ as $value){
                    if($value['codeU'] == $univVoulu){
                        echo "<option value='". $value['codeU'] ."' selected>". $value['nomU'] ."</option>";
                    } else {
                        echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";
                    }
                }
            }
            else {
                foreach ($univ as $value){
                    echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";
                }
            }
            ?>
        </select>
    </div>

    <input type="submit" value="Rechercher les programmes associés" class="btn btn-primary">
</form>

<?php
    if(isset($prog)){
?>
      <table class="table mt-5">
          <thead>
          <tr>
              <th scope="col">#</th>
              <th scope="col">Nom du programme</th>
              <th scope="col">Durée échange</th>
          </tr>
          </thead>
          <tbody>
          <?php

          foreach ($prog as $value){
              echo '<tr>
                        <th scope="row">'. $value['codeProgramme'] .'</th>
                        <td>'. $value['nomProgramme'] .'</td>
                        <td>'. $value['dureeEchange'] .'</td>
                    </tr>';
          }

          ?>
          </tbody>
      </table>
<?php
}
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>
