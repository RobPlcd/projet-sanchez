<h1>Toutes les universités <small class="text-muted">(<?= sizeof($univ); ?>)</small></h1>

<table class="table mt-5">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nom de l'Université</th>
        <th scope="col">Ville</th>
        <th scope="col">Pays</th>
        <th scope="col">Site web</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php

    foreach ($univ as $value){
        echo '<tr>
                        <th scope="row">'. $value['codeU'] .'</th>
                        <td>'. $value['nomU'] .'</td>
                        <td>'. $value['villeU'] .'</td>
                        <td>'. $value['paysU'] .'</td>
                        <td><a href="'. $value['webU'] .'" target="_blank" class="link">'. $value['webU'] .'</a></td>
                        <td><form action="'. base_url() .'index.php/modifierUniv" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeU'] .'" name="codeUniv"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
    }

    ?>
    </tbody>
</table>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>