<h1>Cours par diplômes</h1>

<form action="<?php echo base_url();?>index.php/coursParDiplome" method ="post">
    <div class="form-group">
        <label for="formulaire" class="form-label">Pour le diplôme :</label>
        <select class="form-control" name="codeDiplome">
            <?php
            if(isset($diplomeVoulu)){
                foreach ($diplomes as $value){
                    if($value['codeDiplome'] == $diplomeVoulu){
                        echo "<option value='". $value['codeDiplome'] ."' selected>". $value['nomDiplome'] ."</option>";
                    } else {
                        echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
                    }
                }
            }
            else {
                foreach ($diplomes as $value){
                    echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
                }
            }
            ?>
        </select>
    </div>

    <input type="submit" value="Rechercher les cours associés" class="btn btn-primary">
</form>

<?php
    if(isset($cours)){
?>
      <table class="table mt-5">
          <thead>
          <tr>
              <th scope="col">#</th>
              <th scope="col">Nom du cours</th>
              <th scope="col">Nombre ETCS</th>
              <th scope="col">Année</th>
              <th scope="col">Actions</th>
          </tr>
          </thead>
          <tbody>
          <?php

          foreach ($cours as $value){
              echo '<tr>
                        <th scope="row">'. $value['codeCours'] .'</th>
                        <td>'. $value['LibelleCours'] .'</td>
                        <td>'. $value['nbECTS'] .'</td>
                        <td>'. $value['annee'] .'</td>
                        <td><form action="'. base_url() .'index.php/modifierCours" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeCours'] .'" name="codeCours"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
          }

          ?>
          </tbody>
      </table>
<?php
}
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>
