<h1>Créer un nouveau Diplôme</h1>

<form action="<?php echo base_url();?>index.php/ajouterDiplome" method ="post">
      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du diplôme :</label>
          <input type="text" class="form-control" id="nomDip" placeholder="Nom" name ="nomDiplome" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Niveau du diplôme :</label>
            <input type="text" class="form-control" id="NiveauDip" placeholder="Niveau" name="niveauDiplome" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Université associée :</label>
            <select class="form-control" name="codeU">
                <?php

                    foreach ($univ as $value){
                        echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";
                    }

                ?>
            </select>
      </div>
        <input type="submit" value="Créer" class="btn btn-primary">
</form>


<?php
    // Affichage des succès ou errreurs
    if(isset($confirmation)){
        echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    } else if(isset($erreur)){
        echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
?>