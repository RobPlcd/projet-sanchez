<h1>Détails de la demande n°<?= $mobilite[0]['codeDemandeM']; ?></h1>


<div class="container mt-5">
    <div class="row">
        <div class="col-lg-6">
            <h3 class="mb-2">Détails sur le demandeur</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-user"></i> <?= strtoupper($mobilite[0]['nomEtudiant']); ?> <?= $mobilite[0]['prenomEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-envelope"></i> <?= $mobilite[0]['mailEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-calendar"></i> <?= $mobilite[0]['annee']; ?></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <h3>Détails sur la demande</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-hashtag"></i> Code : <?= $mobilite[0]['codeDemandeM']; ?></li>
                <li class="list-group-item"><i class="fas fa-calendar"></i> Date : <?= $mobilite[0]['dateDepotDemandeM']; ?></li>
            </ul>
        </div>
    </div>
    <div class="mt-5">
        <h3>Détails sur le programme</h3>

        <ul class="list-group mt-2">
            <li class="list-group-item active"><i class="fas fa-circle"></i> Nom : <?= $mobilite[0]['nomProgramme']; ?></li>
            <li class="list-group-item"><i class="fas fa-university"></i> Université d'accueil : <?= $mobilite[0]['nomU']; ?></li>
            <li class="list-group-item"><i class="fas fa-graduation-cap"></i>Diplôme : <?= $mobilite[0]['nomDiplome']; ?></li>
            <li class="list-group-item"><i class="fas fa-city"></i> Ville de l'Université : <?= $mobilite[0]['villeU']; ?></li>
            <li class="list-group-item"><i class="fas fa-map"></i> Pays de l'Université : <?= $mobilite[0]['paysU']; ?></li>
        </ul>
    </div>
    <div class="text-center mt-5">
        <?php

            if($mobilite[0]['etatDemandeM'] == "attente"){
                echo "<form action='". base_url() ."index.php/detailsMobilite' method='post'>
                        <input type='hidden' value='". $mobilite[0]['codeDemandeM'] ."' name='codeDemandeM'>
                        <input type='submit' class=\"btn btn-success\" value='Accepter la demande' name='accepter'>
                    </form>
                    <form action='". base_url() ."index.php/detailsMobilite' method='post'>
                        <input type='hidden' value='". $mobilite[0]['codeDemandeM'] ."' name='codeDemandeM'>
                        <input type='submit' class=\"btn btn-danger\" value='Refuser la demande' name='refuser'>
                    </form> ";
            }

        ?>
    </div>
</div>