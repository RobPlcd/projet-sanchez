<h1>Tous les programmes <small class="text-muted">(<?= sizeof($programme); ?>)</small></h1>

<table class="table mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du programme</th>
            <th scope="col">Durée échange</th>
            <th scope="col">Diplômes associés</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach ($programme as $value){
                echo '<tr>
                        <th scope="row">'. $value['codeProgramme'] .'</th>
                        <td>'. $value['nomProgramme'] .'</td>
                        <td>'. $value['dureeEchange'] .'</td>
                        <td>'. $value['nomUn'] .', '. $value['nomDeux'] .'</td>
                        <td><form action="'. base_url() .'index.php/modifierProgramme" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeProgramme'] .'" name="codeProgramme"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
            }

        ?>
    </tbody>
</table>