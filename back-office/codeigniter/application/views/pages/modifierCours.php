<h1>Modifier le cours <?= $modifier[0]['libelleCours']; ?></h1>

<form action="<?php echo base_url();?>index.php/modifierCours" method ="post">
    <input type="hidden" value="<?= $modifier[0]['codeCours']; ?>" name="code">

      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du Cours :</label>
          <input type="text" class="form-control" id="libelleCours" value="<?= $modifier[0]['libelleCours']; ?>" name="libelleCours" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Nombre ECTS :</label>
            <input type="text" class="form-control" id="nbECTS" value="<?= $modifier[0]['nbECTS']; ?>" name="nbECTS" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Année :</label>
            <input type="text" class="form-control" id="annee" value="<?= $modifier[0]['annee']; ?>" name="annee" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Diplome associé :</label>
            <select class="form-control" name="codeDiplome">
                <?php

                    foreach ($dip as $value){
                        if($value['nomDiplome'] == $modifier[0]['nomDiplome'] ){
                            echo "<option value='". $value['codeDiplome'] ."' selected>". $value['nomDiplome'] ."</option>";
                        } else {
                            echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";  
                        }
                    }

                ?>
            </select>
      </div>
        <input type="submit" value="Modifier <?= $modifier[0]['libelleCours']; ?>" class="btn btn-primary">
</form>
