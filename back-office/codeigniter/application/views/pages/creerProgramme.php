<h1>Créer un nouveau Programme</h1>

<form action="<?php echo base_url();?>index.php/creerProgramme" method ="post">
      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du programme :</label>
          <input type="text" class="form-control" id="nomProgramme" placeholder="nom" name ="nomProgramme" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Durée de l'échange (en jours) :</label>
            <input type="text" class="form-control" id="dureeEchange" placeholder="dureeEchange" name="dureeEchange" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Diplome associé :</label>
            <select class="form-control" name="codeDiplome">
                <?php

                    foreach ($diplome as $value){
                        echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
                    }

                ?>
            </select>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Deuxième diplome associé :</label>
            <select class="form-control" name="codeDiplome_1">
                <?php

                    foreach ($diplome as $value){
                        echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
                    }

                ?>
            </select>
      </div>
        <input type="submit" value="Créer" class="btn btn-primary">
</form>


<?php
    // Affichage des succès ou errreurs
    if(isset($confirmation)){
        echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    } else if(isset($erreur)){
        echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
?>