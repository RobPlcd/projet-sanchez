<h1>Modifier le dipôme <?= $modifier[0]['nomDiplome']; ?></h1>

<form action="<?php echo base_url();?>index.php/modifierDiplome" method ="post">
		<input type="hidden" value="<?= $modifier[0]['codeDiplome']; ?>" name="code">

      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du diplôme :</label>
          <input type="text" class="form-control" id="nomDip" value="<?= $modifier[0]['nomDiplome']; ?>" name="nomDiplome" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Niveau du diplôme :</label>
            <input type="text" class="form-control" id="NiveauDip" value="<?= $modifier[0]['niveauDiplome']; ?>" name="niveauDiplome" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Université associée :</label>
            <select class="form-control" name="codeU">
                <?php

                    foreach ($univ as $value){
                        if($value['nomU'] == $modifier[0]['nomU'] ){
                            echo "<option value='". $value['codeU'] ."' selected>". $value['nomU'] ."</option>";
                        } else {
                            echo "<option value='". $value['codeU'] ."'>". $value['nomU'] ."</option>";  
                        }
                    }

                ?>
            </select>
      </div>
        <input type="submit" value="Modifier <?= $modifier[0]['nomDiplome']; ?>" class="btn btn-primary">
</form>
