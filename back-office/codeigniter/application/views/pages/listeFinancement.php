<h1>Liste des demandes de financement</h1>

<table class="table mt-5 table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Date de la demande</th>
            <th scope="col">Etat</th>
            <th scope="col">Montant demandé</th>
            <th scope="col">Contrat concerné</th>
            <th scope="col">Etudiant demandeur</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
    	<?php

    		foreach ($financement as $value) {
    			echo "<tr>
    				<th scope='row'>". $value['codeDemandeF']  ."</th>
    				<td>". $value['dateDepotDemandeF'] ."</td>";

    			if($value['etatDemandeF'] == "attente"){
    				echo "<td><span class='badge badge-pill badge-info'>". $value['etatDemandeF'] ."</span></td>";
    			} else if($value['etatDemandeF'] == "acceptée"){
    				echo "<td><span class='badge badge-pill badge-success'>". $value['etatDemandeF'] ."</span></td>";
    			} else {
    				echo "<td><span class='badge badge-pill badge-danger'>". $value['etatDemandeF'] ."<span></td>";
    			}

    			echo" <td>". $value['montantDemandeF'] ."€</td>
    				<td>". $value['codeContrat'] ."</td>
    				<td>". $value['nomEtudiant'] ." ". $value['prenomEtudiant'] ."</td>
    				<td><form action='". base_url() ."index.php/detailsFinancement' method='post' class='mt-0 mb-0 pt-0 pb-0'><input name='codeDemandeF' type='hidden' value='". $value['codeDemandeF'] ."'><input type='submit' value='Détails' class='btn-sm btn-secondary'></form></td>
    			</tr>";
    		}

    	?>
    </tbody>
</table>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>