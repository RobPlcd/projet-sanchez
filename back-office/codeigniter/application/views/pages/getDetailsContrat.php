<h1>Détails du contrat n°<?= $contrat[0]['codeContrat']; ?></h1>


<div class="container mt-5">
    <div class="row">
        <div class="col-lg-6">
            <h3 class="mb-2">Détails sur le demandeur</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-user"></i> <?= strtoupper($contrat[0]['nomEtudiant']); ?> <?= $contrat[0]['prenomEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-envelope"></i> <?= $contrat[0]['mailEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-university"></i> Université : <?= $contrat[0]['nomU']; ?></li>
            <li class="list-group-item"><i class="fas fa-graduation-cap"></i>Diplôme suivi : <?= $contrat[0]['nomDiplome']; ?></li>
            <li class="list-group-item"><i class="fas fa-city"></i> Ville : <?= $contrat[0]['villeU']; ?></li>
            <li class="list-group-item"><i class="fas fa-map"></i> Pays: <?= $contrat[0]['paysU']; ?></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <h3>Détails sur le contrat</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-hashtag"></i> Code : <?= $contrat[0]['codeContrat']; ?></li>
                <li class="list-group-item"><i class="fas fa-calendar"></i> Durée du contrat en jours : <?= $contrat[0]['dureeContrat']; ?></li>
                <li class="list-group-item"><i class="fas fa-hashtag"></i> code demande de mobilité liée au contrat : <?= $contrat[0]['codeDemandeM']; ?></li>
            </ul>
        </div>
    </div>
    <div class="text-center mt-5">
        <?php

            if($contrat[0]['etatContrat'] == "attente"){
                echo "<form action='". base_url() ."index.php/detailsContrat' method='post'>
                        <input type='hidden' value='". $contrat[0]['codeContrat'] ."' name='codeContrat'>
                        <input type='submit' class=\"btn btn-success\" value='Accepter la demande' name='accepter'>
                    </form>
                    <form action='". base_url() ."index.php/detailsContrat' method='post'>
                        <input type='hidden' value='". $contrat[0]['codeContrat'] ."' name='codeContrat'>
                        <input type='submit' class=\"btn btn-danger\" value='Refuser la demande' name='refuser'>
                    </form> ";
            }

        ?>
    </div>
</div>