
<div class="pt-5 pb-5 text-center">
    <h1 class="display-4">Interface d'administration</h1>
    <p class="lead text-muted">Contrôlez l'ensemble de l'application via le menu</p>
</div>
<div class="container">
    <div class="row row-cols-3">
        <div class="col">
            <div class="card">
                <img src="<?= asset_url() ?>assets/img/antique-bills-business-cash-210600.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Financements en attente</h5>
                    <p class="card-text">Il y a <span class="badge badge-primary"><?= $nombres['financement'][0]['nbFinancement']; ?></span> financements en attente.</p>
                    <a href="<?= base_url() ?>index.php/listeFinancement" class="btn btn-primary">Aller à Financement</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <img src="<?= asset_url() ?>assets/img/agreement-blur-business-close-up-261679.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Contrats en attente</h5>
                    <p class="card-text">Il y a <span class="badge badge-primary"><?= $nombres['contrats'][0]['nbContrat']; ?></span> contrat(s) en attente.</p>
                    <a href="<?= base_url() ?>index.php/listeContrat" class="btn btn-primary">Aller aux contrats</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <img src="<?= asset_url() ?>assets/img/jet-cloud-landing-aircraft-46148.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Mobilités en attente</h5>
                    <p class="card-text">Il y a <span class="badge badge-primary"><?= $nombres['mobilite'][0]['nbMobilite']; ?></span> demande(s) de mobilité en attente.</p>
                    <a href="<?= base_url() ?>index.php/listeFinancement" class="btn btn-primary">Aller à mobilité</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-center mt-5">
    <small class="text-muted">Programme écrit par <a href="mailto:collaine.louis@gmail.com">Louis Collaine</a> - <a href="mailto:thomaslamothe@free.fr">Thomas Lamothe</a> -
        <a href="mailto:plancader@saliege.fr">Plancade Robin</a></small>
</div>