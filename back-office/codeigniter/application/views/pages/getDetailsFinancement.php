<h1>Détails de la demande n°<?= $financement[0]['codeDemandeF']; ?></h1>


<div class="container mt-5">
    <div class="row">
        <div class="col-lg-6">
            <h3 class="mb-2">Détails sur le demandeur</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-user"></i> <?= strtoupper($financement[0]['nomEtudiant']); ?> <?= $financement[0]['prenomEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-envelope"></i> <?= $financement[0]['mailEtudiant']; ?></li>
                <li class="list-group-item"><i class="fas fa-calendar"></i> <?= $financement[0]['annee']; ?></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <h3>Détails sur la demande</h3>

            <ul class="list-group mt-2">
                <li class="list-group-item active"><i class="fas fa-hashtag"></i> Code : <?= $financement[0]['codeDemandeF']; ?></li>
                <li class="list-group-item"><i class="fas fa-calendar"></i> Date : <?= $financement[0]['dateDepotDemandeF']; ?></li>
                <li class="list-group-item"><i class="fas fa-circle"></i> Etat : <span class="badge badge-info"><?= $financement[0]['etatDemandeF']; ?></span></li>
            </ul>
        </div>
    </div>
    <div class="mt-5">
        <h3>Détails sur le programme</h3>

        <ul class="list-group mt-2">
            <li class="list-group-item active"><i class="fas fa-circle"></i> Nom : <?= $financement[0]['nomProgramme']; ?></li>
            <li class="list-group-item"><i class="fas fa-university"></i> Université d'accueil : <?= $financement[0]['nomU']; ?></li>
            <li class="list-group-item"><i class="fas fa-city"></i> Ville de l'Université : <?= $financement[0]['villeU']; ?></li>
            <li class="list-group-item"><i class="fas fa-map"></i> Pays de l'Université : <?= $financement[0]['paysU']; ?></li>
        </ul>
    </div>
    <div class="text-center mt-5">
        <?php

            if($financement[0]['etatDemandeF'] == "attente"){
                echo "<form action='". base_url() ."index.php/detailsFinancement' method='post'>
                        <input type='number' class='form-control mb-4' placeholder='montant en €' name='montant'>
                        <input type='hidden' value='". $financement[0]['codeDemandeF'] ."' name='codeDemandeF'>
                        <input type='submit' class=\"btn btn-success\" value='Accepter la demande' name='accepter'>
                    </form>
                    <form action='". base_url() ."index.php/detailsFinancement' method='post'>
                        <input type='hidden' value='". $financement[0]['codeDemandeF'] ."' name='codeDemandeF'>
                        <input type='submit' class=\"btn btn-danger\" value='Refuser la demande' name='refuser'>
                    </form> ";
            }

        ?>
    </div>
</div>