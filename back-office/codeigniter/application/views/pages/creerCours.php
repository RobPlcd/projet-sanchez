<h1>Créer un nouveau Cours</h1>

<form action="<?php echo base_url();?>index.php/creerCours" method ="post">
      <div class="form-group">
          <label for="formulaire" class="form-label">Nom du cours :</label>
          <input type="text" class="form-control" id="libelleCo" placeholder="libelle" name ="libelleCours" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Nombres ECTS :</label>
            <input type="text" class="form-control" id="nbECTS" placeholder="nb" name="nbECTS" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Année :</label>
            <input type="text" class="form-control" id="annee" placeholder="annee" name="annee" required>
      </div>
      <div class="form-group">
            <label for="formulaire" class="form-label">Diplome associée :</label>
            <select class="form-control" name="codeDiplome">
                <?php

                    foreach ($nomDip as $value){
                        echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
                    }

                ?>
            </select>
      </div>
        <input type="submit" value="Créer" class="btn btn-primary">
</form>


<?php
    // Affichage des succès ou errreurs
    if(isset($confirmation)){
        echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    } else if(isset($erreur)){
        echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
?>