<h1>Liste des demandes de contrats</h1>

<table class="table mt-5 table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Durée du contrat</th>
            <th scope="col">Etat</th>
            <th scope="col">Etudiant demandeur</th>
            <th scope="col">Demande de mobilité concerné</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
    	<?php

    		foreach ($contrat as $value) {
    			echo "<tr>
    				<th scope='row'>". $value['codeContrat']  ."</th>
    				<td>". $value['dureeContrat'] ."</td>";

    			if($value['etatContrat'] == "à réaliser"){
    				echo "<td><span class='badge badge-pill badge-info'>". $value['etatContrat'] ."</span></td>";
    			} else if($value['etatContrat'] == "en cours"){
    				echo "<td><span class='badge badge-pill badge-success'>". $value['etatContrat'] ."</span></td>";
    			} else {
    				echo "<td><span class='badge badge-pill badge-danger'>". $value['etatContrat'] ."<span></td>";
    			}

    			echo" <td>". $value['nomEtudiant'] ." ". $value['prenomEtudiant'] ."</td>
                    <td>". $value['codeDemandeM'] ."</td>
    				<td><form action='". base_url() ."index.php/detailsContrat' method='post' class='mt-0 mb-0 pt-0 pb-0'><input name='codeContrat' type='hidden' value='". $value['codeContrat'] ."'><input type='submit' value='Détails' class='btn-sm btn-secondary'></form></td>
    			</tr>";
    		}

    	?>
    </tbody>
</table>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>