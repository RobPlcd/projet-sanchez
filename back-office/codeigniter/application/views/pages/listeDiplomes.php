<h1>Tous les diplômes <small class="text-muted">(<?= sizeof($diplome); ?>)</small></h1>

<table class="table mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du diplôme</th>
            <th scope="col">Niveau du diplôme</th>
            <th scope="col">Université associée</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php

            foreach ($diplome as $value){
                echo '<tr>
                        <th scope="row">'. $value['codeDiplome'] .'</th>
                        <td>'. $value['nomDiplome'] .'</td>
                        <td>'. $value['niveauDiplome'] .'</td>
                        <td>'. $value['nomU'] .'</td>
                        <td><form action="'. base_url() .'index.php/modifierDiplome" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeDiplome'] .'" name="codeDiplome"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
            }

        ?>
    </tbody>
</table>

<?php
    // Affichage des succès ou errreurs
    if(isset($confirmation)){
        echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    } else if(isset($erreur)){
        echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
?>