<h1>Tous les cours <small class="text-muted">(<?= sizeof($cours); ?>)</small></h1>

<table class="table mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du cours</th>
            <th scope="col">Nombre ETCS</th>
            <th scope="col">Année</th>
            <th scope="col">Diplôme associé</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php

            foreach ($cours as $value){
                echo '<tr>
                        <th scope="row">'. $value['codeCours'] .'</th>
                        <td>'. $value['libelleCours'] .'</td>
                        <td>'. $value['nbECTS'] .'</td>
                        <td>'. $value['annee'] .'</td>
                        <td>'. $value['nomDiplome'] .'</td>
                        <td><form action="'. base_url() .'index.php/modifierCours" method="POST" class="mt-0 mb-0 pt-0 pb-0"> <input type="hidden" value="'. $value['codeCours'] .'" name="codeCours"><input type="submit" value="Modifier" class="btn-sm btn-primary"></form></td>
                    </tr>';
            }

        ?>
    </tbody>
</table>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>