<h1>Supprimer un diplôme</h1>

<form action="<?php echo base_url();?>index.php/supprimerDiplome" method ="post">
    <div class="form-group">
        <label for="formulaire" class="form-label">Diplôme à supprimer :</label>
        <select class="form-control" name="nomDiplome">
            <?php

            foreach ($diplome as $value){
                echo "<option value='". $value['codeDiplome'] ."'>". $value['nomDiplome'] ."</option>";
            }

            ?>
        </select>
        <small class="form-text text-muted">Cette opération est irréversible.</small>
    </div>

    <input type="submit" value="Supprimer" class="btn btn-danger">
</form>

<?php
// Affichage des succès ou errreurs
if(isset($confirmation)){
    echo '<div class="fixedAlert alert alert-success alert-dismissible fade show" role="alert">
              '. $confirmation .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
} else if(isset($erreur)){
    echo '<div class="fixedAlert alert alert-danger alert-dismissible fade show" role="alert">
              '. $erreur .'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
}
?>