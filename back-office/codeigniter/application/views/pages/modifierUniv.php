<h1>Modifier <?= $univ[0]['nomU']; ?></h1>

<form action="<?php echo base_url();?>index.php/modifierUniv" method ="post">
    <input type="hidden" value="<?= $univ[0]['codeU']; ?>" name="code">

    <div class="form-group">
        <label for="formulaire" class="form-label">Nom de l'université :</label>
        <input type="text" class="form-control" value="<?= $univ[0]['nomU']; ?>" name="nomUniv" required>
    </div>
    <div class="form-group">
        <label for="formulaire" class="form-label">Ville :</label>
        <input type="text" class="form-control" value="<?= $univ[0]['villeU']; ?>" name="villeUniv" required>
    </div>
    <div class="form-group">
        <label for="formulaire" class="form-label">Pays :</label>
        <input type="text" class="form-control" value="<?= $univ[0]['paysU']; ?>" name="paysUniv" required>
    </div>
    <div class="form-group">
        <label for="formulaire" class="form-label">Site web :</label>
        <input type="text" class="form-control" value="<?= $univ[0]['webU']; ?>" name="webUniv" required>
    </div>

    <input type="submit" value="Modifier <?= $univ[0]['nomU']; ?>" class="btn btn-primary">
</form>
