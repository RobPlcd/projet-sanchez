
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Gestion de la mobilite Etudiante</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>assets/style.css">
</head>
<body>
	<!-- Juste le Header en Bootstrap -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<!-- Partie Responsive -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  	</button>
	  	<!-- Partie Principale -->
	  	<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    	<a class="navbar-brand" href="<?= base_url(); ?>">Gestion de la Mobilité des Etudiants</a>
	    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Diplomes</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/ajouterDiplome">Créer un diplôme</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/supprimerDiplome">Supprimer un Diplôme</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/listDiplome">Liste des diplômes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Programmes</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/creerProgramme">Créer un programme</a>
                        <a class="dropdown-item" href="<?=base_url(); ?>index.php/supprimerProgramme">Supprimer un programme</a>
                        <a class="dropdown-item" href="<?=base_url(); ?>index.php/listProgramme">Liste des programmes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cours</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/creerCours">Créer un cours</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/supprimerCours">Supprimer un cours</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/listCours">Liste des cours</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Universités</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/creerUniv">Créer une université</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/supprimerUniv">Supprimer une université</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/listUniv">Liste des universités</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Recherche</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/progParUniv">Programmes par université</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/coursParDiplome">Cours par diplôme</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>index.php/diplomesParUniv">Diplomes par Universités</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url(); ?>index.php/listeFinancement">Financement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url(); ?>index.php/listeMobilite/">Mobilité</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url(); ?>index.php/listeContrat/">Contrats</a>
                </li>
	    	</ul>
	  	</div>
	</nav>

	<!-- Le container principal -->
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-9 special-admin">
