<?php

class Programme_model extends CI_model{

	public function __construct(){
		$this->load->database();
	}

	public function getDiplomeDetails(){
        $sql='SELECT diplomes.codeDiplome, diplomes.nomDiplome
            FROM diplomes
            ORDER BY diplomes.nomDiplome';

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getAllProg(){
	    $sql = "SELECT programmes.codeProgramme, programmes.nomProgramme, programmes.dureeEchange, v1.nomDiplome as nomUn ,v2.nomDiplome as nomDeux
				FROM programmes,diplomes as v1,diplomes as v2
				WHERE programmes.codeDiplome = v1.codeDiplome
				AND programmes.codeDiplome_1 = v2.codeDiplome";

	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

	//insère un nouveau Programme dans la base
	public function creationProg($nomProgramme, $dureeEchange, $codeDiplome, $codeDiplome_1){
		$sql='INSERT INTO programmes VALUES("",'. $this->db->escape($nomProgramme) .',"'.$dureeEchange.'", "'.$codeDiplome.'","'.$codeDiplome_1.'")';
		$result = $this->db->query($sql);
	}

	public function getcodeProgAndName(){
        $sql='SELECT codeProgramme, nomProgramme
            FROM programmes
            ORDER BY nomProgramme';

        $result = $this->db->query($sql);
        return $result->result_array();
    }

	 public function deleteProg($codeProgramme){
		$sql='DELETE FROM programmes WHERE programmes.codeProgramme = '.$codeProgramme.';';
		$result = $this->db->query($sql);
	}

	public function getProgByCode($id){
        $sql='SELECT programmes.codeProgramme, programmes.nomProgramme, programmes.dureeEchange, v1.nomDiplome as nomUn ,v2.nomDiplome as nomDeux
				FROM programmes,diplomes as v1,diplomes as v2
				WHERE programmes.codeDiplome = v1.codeDiplome
				AND programmes.codeDiplome_1 = v2.codeDiplome
                AND programmes.codeProgramme = '. $id;

        $result = $this->db->query($sql);
        return $result->result_array();
    }

	public function modifierProg($id, $nomProgramme, $dureeEchange, $nomUn, $nomDeux){
		$sql = "UPDATE programmes
				SET nomProgramme = ". $this->db->escape($nomProgramme) .",
					dureeEchange = ". $dureeEchange .",
					codeDiplome = ". $nomUn .",
					codeDiplome_1 = ". $nomDeux ."
				WHERE codeProgramme = ". $id;
		$this->db->query($sql);
	}

	public function progParUniv($codeU) {
        $sql = "SELECT programmes.codeProgramme, programmes.nomProgramme, programmes.dureeEchange
            FROM programmes,universites,diplomes
            WHERE programmes.codeDiplome = diplomes.codeDiplome
            AND universites.codeU = diplomes.codeU
            AND universites.codeU = " .$codeU;

        return $this->db->query($sql)->result_array();
    }

    public function getAllUniv(){
	    $sql = "SELECT codeU, nomU
	            FROM universites
	            ORDER BY codeU";

	    return $this->db->query($sql)->result_array();
    }
}
