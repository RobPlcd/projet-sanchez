<?php 

class Cours_model extends CI_model{

	public function __construct(){
		$this->load->database();
	}

	public function allCours(){
		$query = $this->db->get('cours');
		return $query->result_array();
	}

	public function findCours($libelleCours){
		$sql='SELECT cours.LibelleCours
				FROM cours, diplomes
				WHERE cours.codeDiplome=diplomes.codeDiplome
				AND diplomes.nomDiplome="'.$libelleCours.'";';

		$result = $this->db->query($sql);
		return $result->result_array();
	}

	public function getDipDetails(){
        $sql='SELECT diplomes.codeDiplome, diplomes.nomDiplome
            FROM diplomes
            ORDER BY diplomes.nomDiplome';

        $result = $this->db->query($sql);
        return $result->result_array();
    }
//insère un nouveau diplome dans la base
	public function creationCours($libelleCours, $nbECTS, $annee, $codeDiplome){
		$sql='INSERT INTO cours VALUES("","'.$libelleCours.'","'.$nbECTS.'", "'.$annee.'", "'.$codeDiplome.'")';
		$result = $this->db->query($sql);
	}

	public function getcodeCoursAndName(){
        $sql='SELECT codeCours, libelleCours
            FROM cours
            ORDER BY libelleCours';

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    //supprim diplome dans la base
	public function deleteCours($codeCours){
		$sql='DELETE FROM cours WHERE cours.codeCours = '.$codeCours.';';
		$result = $this->db->query($sql);
	}

	public function getAllCours(){
	    $sql = "SELECT cours.codeCours, cours.libelleCours, cours.nbECTS, cours.annee, diplomes.nomDiplome
                FROM cours, diplomes 
                WHERE cours.codeDiplome = diplomes.codeDiplome
                ORDER BY cours.codeDiplome";

	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

     public function getCoursByCode($id){
        $sql='SELECT cours.codeCours, cours.libelleCours, cours.nbECTS, cours.annee, diplomes.nomDiplome 
                FROM cours, diplomes 
                WHERE cours.codeDiplome = diplomes.codeDiplome
                AND cours.codeCours = '. $id;

        $result = $this->db->query($sql);
        return $result->result_array();
    }

	public function modifierCours($id, $libelleCours, $nbECTS, $annee, $codeDiplome){
		$sql = "UPDATE cours
				SET libelleCours = ". $this->db->escape($libelleCours) .",
					nbECTS = ". $this->db->escape($nbECTS) .",
					annee = ". $this->db->escape($annee) .",
					codeDiplome = ". $codeDiplome ."
				WHERE codeCours = ". $id;
		$this->db->query($sql);			
	}

}