<?php

class Diplomes_model extends CI_model{

	public function __construct(){
		$this->load->database();
	}

	//Afficher les diplomes par univrsité
	public function findDip($nomU){
		$sql='SELECT diplomes.nomDiplome
				FROM diplomes, universites
				WHERE diplomes.codeU=universites.codeU
				AND universites.nomU= "'.$nomU.'";';

		$result = $this->db->query($sql);
		return $result->result_array();
	}

	// Afficher les cours en fonction d'un diplome
    public function coursParDiplome($codeDiplome){
	    $sql = "SELECT codeCours, LibelleCours, nbECTS, annee
	            FROM cours
	            WHERE cours.codeDiplome = ". $codeDiplome ."
	            ORDER BY codeCours";

	    return $this->db->query($sql)->result_array();
    }

    // Afficher les cours en fonction d'un diplome
    public function diplomesParUniv($codeUniv){
        $sql = "SELECT codeDiplome, nomDiplome, niveauDiplome
	            FROM diplomes
	            WHERE codeU = ". $codeUniv ."
	            ORDER BY codeU";

        return $this->db->query($sql)->result_array();
    }

	public function getUnivDetails(){
        $sql='SELECT universites.codeU, universites.nomU
            FROM universites
            ORDER BY universites.nomU';

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getAllDiplomes(){
	    $sql = "SELECT diplomes.codeDiplome, diplomes.nomDiplome, diplomes.niveauDiplome, universites.nomU
                FROM diplomes, universites
                WHERE diplomes.codeU = universites.codeU
                ORDER BY diplomes.codeDiplome";

	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

	//insère un nouveau diplome dans la base
	public function creationDip($nomDiplome, $niveauDiplome, $codeU){
		$sql='INSERT INTO diplomes VALUES("","'.$nomDiplome.'","'.$niveauDiplome.'", "'.$codeU.'")';
		$result = $this->db->query($sql);
	}

	public function getcodeDiplomesAndName(){
        $sql='SELECT codeDiplome, nomDiplome
            FROM diplomes
            ORDER BY nomDiplome';

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getDiplomeByCode($id){
        $sql='SELECT diplomes.codeDiplome, diplomes.nomDiplome, diplomes.niveauDiplome, universites.nomU
                FROM diplomes, universites
                WHERE diplomes.codeU = universites.codeU
                AND diplomes.codeDiplome = '. $id;

        $result = $this->db->query($sql);
        return $result->result_array();
    }

	public function deleteDip($codeDiplome){
		$sql='DELETE FROM diplomes WHERE diplomes.codeDiplome = '.$codeDiplome.';';
		$result = $this->db->query($sql);
	}

	public function modifierDiplome($id, $nomDiplome, $valeurDiplome, $codeUniv){
		$sql = "UPDATE diplomes
				SET nomDiplome = ". $this->db->escape($nomDiplome) .",
					niveauDiplome = ". $this->db->escape($valeurDiplome) .",
					codeU = ". $codeUniv ."
				WHERE codeDiplome = ". $id;
		$this->db->query($sql);
	}


  public function dipParUniv($codeU) {
    $sql = "SELECT diplomes.codeDiplome, diplomes.nomDiplome
            FROM universites,diplomes
            WHERE universites.codeU = diplomes.codeU
            AND universites.codeU = " .$codeU;


    return $this->db->query($sql)->result_array();


  }

}
