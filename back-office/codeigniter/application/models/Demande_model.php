<?php 

class Demande_model extends CI_model{

	public function __construct(){
		$this->load->database();
	}

	public function allFinanceAndDetails(){
		$sql = "SELECT codeDemandeF, dateDepotDemandeF, etatDemandeF, montantDemandeF, codeContrat, etudiants.nomEtudiant, etudiants.prenomEtudiant
				FROM demandesfinancement, etudiants
				WHERE etudiants.numEtudiant = demandesfinancement.numEtudiant";

		$resultat = $this->db->query($sql);
		return $resultat->result_array();
	}

	public function getDetailsById($id){
	    $sql = "SELECT codeDemandeF, dateDepotDemandeF, etatDemandeF, montantDemandeF, 
                etudiants.nomEtudiant, etudiants.prenomEtudiant, etudiants.mailEtudiant, 
                etudiants.annee, demandesmobilite.dateDepotDemandeM, demandesmobilite.etatDemandeM, 
                programmes.nomProgramme, universites.nomU, universites.villeU, universites.paysU
                FROM demandesfinancement, etudiants, contrats, demandesmobilite, programmes, diplomes, universites 
                WHERE etudiants.numEtudiant = demandesfinancement.numEtudiant
                AND demandesfinancement.codeContrat = contrats.codeContrat
                AND contrats.codeDemandeM = demandesmobilite.codeDemandeM
                AND programmes.codeProgramme = demandesmobilite.codeProgramme
                AND programmes.codeDiplome_1 = diplomes.codeDiplome
                AND universites.codeU = diplomes.codeU
                AND codeDemandeF = ". $id;
	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

    public function accepterDemandeFinancement($id, $montant){
	    $sql = "UPDATE demandesfinancement
	            SET etatDemandeF  = 'acceptée',
	            	montantDemandeF = ". $montant ."
	            WHERE codeDemandeF = ". $id;
	    $this->db->query($sql);
    }

    public function refuserDemandeFinancement($id){
        $sql = "UPDATE demandesfinancement
	            SET etatDemandeF  = 'refusée'
	            WHERE codeDemandeF = ". $id;
        $this->db->query($sql);
    }
     public function allMobiliteAndDetails(){
		$sql = "SELECT codeDemandeM, dateDepotDemandeM, etatDemandeM, codeProgramme, etudiants.nomEtudiant, etudiants.prenomEtudiant
				FROM demandesmobilite, etudiants
				WHERE etudiants.numEtudiant = demandesmobilite.numEtudiant";

		$resultat = $this->db->query($sql);
		return $resultat->result_array();
	}

	public function getDetailsMobiliteById($id){
	    $sql ="SELECT codeDemandeM, dateDepotDemandeM, etatDemandeM, 
                etudiants.nomEtudiant, etudiants.prenomEtudiant, etudiants.mailEtudiant, 
                etudiants.annee, programmes.nomProgramme, diplomes.nomDiplome, universites.nomU, universites.villeU, universites.paysU
                FROM etudiants, demandesmobilite, programmes, diplomes, universites 
                WHERE etudiants.numEtudiant = demandesmobilite.numEtudiant
                AND demandesmobilite.codeProgramme = programmes.codeProgramme
               AND programmes.codeDiplome_1 = diplomes.codeDiplome
                AND universites.codeU = diplomes.codeU
                AND codeDemandeM =". $id;
	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

    public function accepterDemandeMobilite($id){
	    $sql = "UPDATE demandesmobilite
	            SET etatDemandeM  = 'acceptée'
	            WHERE codeDemandeM = ". $id;
	    $this->db->query($sql);
    }

    public function refuserDemandeMobilite($id){
        $sql = "UPDATE demandesmobilite
	            SET etatDemandeM  = 'refusée'
	            WHERE codeDemandeM = ". $id;
        $this->db->query($sql);
    }

    public function allContratAndDetails(){
		$sql = "SELECT contrats.codeContrat, contrats.dureeContrat, contrats.etatContrat, contrats.codeDemandeM, etudiants.nomEtudiant, etudiants.prenomEtudiant
				FROM contrats, demandesmobilite, etudiants				
				WHERE contrats.codeDemandeM=demandesmobilite.codeDemandeM
				AND demandesmobilite.numEtudiant=etudiants.numEtudiant";            
                

		$resultat = $this->db->query($sql);
		return $resultat->result_array();
	}
		public function getDetailsContratById($id){
	    $sql ="SELECT contrats.codeContrat, contrats.dureeContrat, contrats.etatContrat, contrats.codeDemandeM, etudiants.nomEtudiant, etudiants.prenomEtudiant, etudiants.mailEtudiant, diplomes.nomDiplome, universites.nomU, universites.villeU, universites.paysU
                FROM contrats,etudiants, demandesmobilite,diplomes, universites 
                WHERE contrats.codeDemandeM=demandesmobilite.codeDemandeM
                AND demandesmobilite.numEtudiant=etudiants.numEtudiant
                AND etudiants.codeDiplome=diplomes.codeDiplome
                AND diplomes.codeU=universites.codeU
                AND codeContrat =". $id;
	    $result = $this->db->query($sql);
	    return $result->result_array();
    }

    public function accepterDemandeContrat($id){
	    $sql = "UPDATE contrats
	            SET etatContrat  = 'acceptée'
	            WHERE codeContrat = ". $id;
	    $this->db->query($sql);
    }

    public function refuserDemandeContrat($id){
        $sql = "UPDATE contrats
	            SET etatContrat  = 'refusée'
	            WHERE codeContrat = ". $id;
        $this->db->query($sql);
    }
}