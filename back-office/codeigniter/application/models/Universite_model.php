<?php

class Universite_model extends CI_Model{

    public function __construct(){
        $this->load->database();
    }

    public function creationUniv($nomUniv, $villeUniv, $paysUniv, $webUniv){
        $sql='INSERT INTO universites
              VALUES("","'. $nomUniv .'","'. $villeUniv .'", "'. $paysUniv .'","'. $webUniv .'")';
        $this->db->query($sql);
    }

    public function getcodeUAndName(){
        $sql = "SELECT codeU, nomU
                FROM universites";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function deleteUniv($id){
        $sql = "DELETE FROM universites WHERE codeU = ". $id;
        $this->db->query($sql);
    }

    public function getAllUniv(){
        $sql = "SELECT codeU, nomU, villeU, paysU, webU
                FROM universites
                ORDER BY codeU";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getAnUniv($id){
        $sql = "SELECT codeU, nomU, villeU, paysU, webU
                FROM universites
                WHERE codeU = ". $id;
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function modifierUniv($id, $nom, $ville, $pays, $web){
        $sql = "UPDATE universites
                SET nomU = ". $this->db->escape($nom) .",
                    villeU = ". $this->db->escape($ville) .",
                    paysU = ". $this->db->escape($pays) .",
                    webU = ". $this->db->escape($web) ."
                WHERE codeU = ". $id;
        $this->db->query($sql);
    }

}
?>