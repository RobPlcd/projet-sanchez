<?php

class Demarrage_model extends CI_model{

    public function __construct()
    {
        $this->load->database();
    }

    public function getNumbers(){
        $sql = "SELECT COUNT(demandesfinancement.codeDemandeF) as nbFinancement
                FROM demandesfinancement
                WHERE etatDemandeF = \"attente\"";
        $result['financement'] = $this->db->query($sql)->result_array();

        $sql = "SELECT COUNT(demandesmobilite.codeDemandeM) as nbMobilite
                FROM demandesmobilite
                WHERE etatDemandeM = \"attente\"";

        $result['mobilite'] = $this->db->query($sql)->result_array();

        $sql = "SELECT COUNT(contrats.codeContrat) as nbContrat
                FROM contrats
                WHERE etatContrat = \"attente\"";

        $result['contrats'] = $this->db->query($sql)->result_array();

        return $result;
    }

}