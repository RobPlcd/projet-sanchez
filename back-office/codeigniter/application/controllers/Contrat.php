<?php

class Contrat extends CI_Controller{

  public function listeContrat(){
    $this->load->model('Demande_model');

    $data['contrat'] = $this->Demande_model->allContratAndDetails();

    $this->load->view('header');
    $this->load->view('pages/listeContrat', $data);
    $this->load->view('footer');
  }
  public function getAllContratDetails(){

        if(isset($_POST['accepter'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->accepterDemandeContrat($_POST['codeContrat']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['contrat'] = $this->Demande_model->allContratAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeContrat', $data);
            $this->load->view('footer');

        } elseif (isset($_POST['refuser'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->refuserDemandeContrat($_POST['codeContrat']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['contrat'] = $this->Demande_model->allContratAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeContrat', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Demande_model');

            $data['contrat'] = $this->Demande_model->getDetailsContratById($_POST['codeContrat']);

            $this->load->view('header');
            $this->load->view('pages/getDetailsContrat', $data);
            $this->load->view('footer');
        }
    }
}

