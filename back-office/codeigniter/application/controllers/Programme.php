<?php

class Programme extends CI_Controller{

      public function creationProg(){

            if (isset($_POST['nomProgramme'])){
                  $this->load->model('Programme_model');

                  try{
                      $this->Programme_model->creationProg($_POST['nomProgramme'],$_POST['dureeEchange'], $_POST['codeDiplome'], $_POST['codeDiplome_1']);
                      $data['confirmation'] = "Opération effectuée avec succès";
                  } catch(Exception $e){
                      $data['erreur'] = 'Echec de l\'opération';
                  }

                  $data['diplome'] = $this->Programme_model->getDiplomeDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerProgramme', $data);
                  $this->load->view('footer');
            } else {
                    $this->load->model('Programme_model');
                    $data['diplome'] = $this->Programme_model->getDiplomeDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerProgramme', $data);
                  $this->load->view('footer');
            }
      }

      public function deleteProg(){

          if(isset($_POST['nomProgramme'])){
              $this->load->model('Programme_model');

              try{
                  $this->Programme_model->deleteProg($_POST['nomProgramme']);
                  $data['confirmation'] = "Opération effectuée avec succès";
              } catch(Exception $e){
                  $data['erreur'] = 'Echec de l\'opération';
              }

              $data['programme'] = $this->Programme_model->getcodeProgAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerProgramme', $data);
              $this->load->view('footer');
          } else {
              $this->load->model('Programme_model');

              $data['programme'] = $this->Programme_model->getcodeProgAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerProgramme', $data);
              $this->load->view('footer');
          }
      }

      public function allProg(){
          $this->load->model('Programme_model');

          $data['programme'] = $this->Programme_model->getAllProg();

          $this->load->view('header');
          $this->load->view('pages/listeProgramme', $data);
          $this->load->view('footer');

      }

    public function modifierProg(){
        // si on a envoyé un ordre de modification
        if(isset($_POST['nomProgramme'])){
            $this->load->model('Programme_model');

            try{
                $this->Programme_model->modifierProg($_POST['code'], $_POST['nomProgramme'], $_POST['dureeEchange'], $_POST['nomUn'], $_POST['nomDeux']);
                $data['confirmation'] = "Le programme a bien été modifié";
            } catch(Exception $e){
               $data['erreur'] = "Echec lors de la modification";
            }

            $data['programme'] =  $this->Programme_model->getAllProg();

            $this->load->view('header');
            $this->load->view('pages/listeProgramme', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Programme_model');

            $data['modifier'] = $this->Programme_model->getProgByCode($_POST['codeProgramme']);
            $data['diplome'] = $this->Programme_model->getDiplomeDetails();

            $this->load->view('header');
            $this->load->view('pages/modifierProgramme', $data);
            $this->load->view('footer');
        }
    }

    public function progParUniv(){
          if(isset($_POST['codeU'])){
              $this->load->model('Programme_model');

              try{
                  $data['prog'] = $this->Programme_model->progParUniv($_POST['codeU']);
                  $data['confirmation'] = "Affichage des programmes avec succes";
              } catch(Exception $e){
                  $data['erreur'] = "Erreur lors de la recuperation des infos";
              }

              $data['univVoulu'] = $_POST['codeU'];
              $data['univ'] = $this->Programme_model->getAllUniv();

              $this->load->view('header');
              $this->load->view('pages/progParUniv', $data);
              $this->load->view('footer');
          } else {
              $this->load->model('Programme_model');

              $data['univ'] = $this->Programme_model->getAllUniv();

              $this->load->view('header');
              $this->load->view('pages/progParUniv', $data);
              $this->load->view('footer');
          }
    }
}

?>
