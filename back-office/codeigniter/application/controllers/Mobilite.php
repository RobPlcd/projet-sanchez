<?php

class Mobilite extends CI_Controller{

	public function listeMobilite(){
		$this->load->model('Demande_model');

		$data['mobilite'] = $this->Demande_model->allMobiliteAndDetails();

		$this->load->view('header');
		$this->load->view('pages/listeMobilite', $data);
		$this->load->view('footer');
	}

	public function getAllMobiliteDetails(){

	    if(isset($_POST['accepter'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->accepterDemandeMobilite($_POST['codeDemandeM']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['mobilite'] = $this->Demande_model->allMobiliteAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeMobilite', $data);
            $this->load->view('footer');

        } elseif (isset($_POST['refuser'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->refuserDemandeMobilite($_POST['codeDemandeM']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['mobilite'] = $this->Demande_model->allMobiliteAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeMobilite', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Demande_model');

            $data['mobilite'] = $this->Demande_model->getDetailsMobiliteById($_POST['codeDemandeM']);

            $this->load->view('header');
            $this->load->view('pages/getDetailsMobilite', $data);
            $this->load->view('footer');
        }
    }

}