<?php 

class Cours extends CI_Controller{

    public function creationCours(){

            if (isset($_POST['libelleCours'])){
                  $this->load->model('Cours_model');

                  try{
                      $this->Cours_model->creationCours($_POST['libelleCours'],$_POST['nbECTS'], $_POST['annee'], $_POST['codeDiplome']);
                      $data['confirmation'] = "Opération effectuée avec succès";
                  } catch(Exception $e){
                      $data['erreur'] = 'Echec de l\'opération';
                  }

                  $data['nomDip'] = $this->Cours_model->getDipDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerCours', $data);
                  $this->load->view('footer');
            } else {
                    $this->load->model('Cours_model');
                    $data['nomDip'] = $this->Cours_model->getDipDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerCours', $data);
                  $this->load->view('footer');
            }
      }

      public function deleteCours(){

          if(isset($_POST['libelleCours'])){
              $this->load->model('Cours_model');

              try{
                  $this->Cours_model->deleteCours($_POST['libelleCours']);
                  $data['confirmation'] = "Opération effectuée avec succès";
              } catch(Exception $e){
                  $data['erreur'] = 'Echec de l\'opération';
              }

              $data['cours'] = $this->Cours_model->getcodeCoursAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerCours', $data);
              $this->load->view('footer');
          } else {
              $this->load->model('Cours_model');

              $data['cours'] = $this->Cours_model->getcodeCoursAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerCours', $data);
              $this->load->view('footer');
          }
      }

      public function allCours(){
          $this->load->model('Cours_model');

          $data['cours'] = $this->Cours_model->getAllCours();

          $this->load->view('header');
          $this->load->view('pages/listeCours', $data);
          $this->load->view('footer');

      }

      public function modifierCours(){
        // si on a envoyé un ordre de modification
        if(isset($_POST['libelleCours'])){
            $this->load->model('Cours_model');

            try{
                $this->Cours_model->modifierCours($_POST['code'], $_POST['libelleCours'], $_POST['nbECTS'], $_POST['annee'], $_POST['codeDiplome']);
                $data['confirmation'] = "Le cours a bien été modifié";
            } catch(Exception $e){
               $data['erreur'] = "Echec lors de la modification"; 
            }

            $data['cours'] =  $this->Cours_model->getAllCours();

            $this->load->view('header');
            $this->load->view('pages/listeCours', $data);
            $this->load->view('footer');            
        } else {
            $this->load->model('Cours_model');

            $data['modifier'] = $this->Cours_model->getCoursByCode($_POST['codeCours']);
            $data['dip'] = $this->Cours_model->getdipDetails();

            $this->load->view('header');
            $this->load->view('pages/modifierCours', $data);
            $this->load->view('footer');
        }
    }

}