<?php

class Universites extends CI_Controller{

    public function creationUniv(){

        if (isset($_POST['nomUniv'])){
            $this->load->model('Universite_model');

            try{
                $this->Universite_model->creationUniv($_POST['nomUniv'],$_POST['villeUniv'], $_POST['paysUniv'], $_POST['webUniv']);
                $data['confirmation'] = "Opération effectuée avec succès";
            } catch(Exception $e){
                $data['erreur'] = 'Echec de l\'opération';
            }

            $this->load->view('header');
            $this->load->view('pages/creerUniv', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Diplomes_model');

            $this->load->view('header');
            $this->load->view('pages/creerUniv');
            $this->load->view('footer');
        }
    }

    public function deleteUniv(){

        if(isset($_POST['nomUniv'])){
            $this->load->model('Universite_model');

            try{
                $this->Universite_model->deleteUniv($_POST['nomUniv']);
                $data['confirmation'] = "Opération effectuée avec succès";
            } catch(Exception $e){
                $data['erreur'] = 'Echec de l\'opération';
            }

            $data['univ'] = $this->Universite_model->getcodeUAndName();

            $this->load->view('header');
            $this->load->view('pages/supprimerUniv', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Universite_model');

            $data['univ'] = $this->Universite_model->getcodeUAndName();

            $this->load->view('header');
            $this->load->view('pages/supprimerUniv', $data);
            $this->load->view('footer');
        }
    }

    public function allUniv(){
        $this->load->model('Universite_model');

        $data['univ'] = $this->Universite_model->getAllUniv();

        $this->load->view('header');
        $this->load->view('pages/listeUniv', $data);
        $this->load->view('footer');

    }

    public function modifierUniv(){
        // si on a envoyé un ordre de modification
        if(isset($_POST['nomUniv'])){
            $this->load->model('Universite_model');

            try{
                $this->Universite_model->modifierUniv($_POST['code'], $_POST['nomUniv'], $_POST['villeUniv'], $_POST['paysUniv'], $_POST['webUniv']);
                $data['confirmation'] = "L'université' a bien été modifié";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la modification";
            }

            $data['univ'] = $this->Universite_model->getAllUniv();

            $this->load->view('header');
            $this->load->view('pages/listeUniv', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Universite_model');

            $data['univ'] = $this->Universite_model->getAnUniv($_POST['codeUniv']);

            $this->load->view('header');
            $this->load->view('pages/modifierUniv', $data);
            $this->load->view('footer');
        }
    }
}

?>