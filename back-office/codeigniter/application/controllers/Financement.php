<?php

class Financement extends CI_Controller{

	public function listeFinancement(){
		$this->load->model('Demande_model');

		$data['financement'] = $this->Demande_model->allFinanceAndDetails();

		$this->load->view('header');
		$this->load->view('pages/listeFinancement', $data);
		$this->load->view('footer');
	}

	public function getAllDetails(){

	    if(isset($_POST['accepter'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->accepterDemandeFinancement($_POST['codeDemandeF'], $_POST['montant']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['financement'] = $this->Demande_model->allFinanceAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeFinancement', $data);
            $this->load->view('footer');

        } elseif (isset($_POST['refuser'])){
            $this->load->model('Demande_model');

            try{
                $this->Demande_model->refuserDemandeFinancement($_POST['codeDemandeF']);
                $data['confirmation'] = "L'état de la demande a été mis à jour";
            } catch(Exception $e){
                $data['erreur'] = "Echec lors de la mise à jour de la demande";
            }
            $data['financement'] = $this->Demande_model->allFinanceAndDetails();

            $this->load->view('header');
            $this->load->view('pages/listeFinancement', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Demande_model');

            $data['financement'] = $this->Demande_model->getDetailsById($_POST['codeDemandeF']);

            $this->load->view('header');
            $this->load->view('pages/getDetailsFinancement', $data);
            $this->load->view('footer');
        }
    }

}