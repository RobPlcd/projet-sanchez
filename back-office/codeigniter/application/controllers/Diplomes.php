<?php

class Diplomes extends CI_Controller{

      public function creationDip(){

            if (isset($_POST['nomDiplome'])){
                  $this->load->model('Diplomes_model');

                  try{
                      $this->Diplomes_model->creationDip($_POST['nomDiplome'],$_POST['niveauDiplome'], $_POST['codeU']);
                      $data['confirmation'] = "Opération effectuée avec succès";
                  } catch(Exception $e){
                      $data['erreur'] = 'Echec de l\'opération';
                  }

                  $data['univ'] = $this->Diplomes_model->getUnivDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerDiplome', $data);
                  $this->load->view('footer');
            } else {
                    $this->load->model('Diplomes_model');
                    $data['univ'] = $this->Diplomes_model->getUnivDetails();

                  $this->load->view('header');
                  $this->load->view('pages/creerDiplome', $data);
                  $this->load->view('footer');
            }
      }

      public function deleteDip(){

          if(isset($_POST['nomDiplome'])){
              $this->load->model('Diplomes_model');

              try{
                  $this->Diplomes_model->deleteDip($_POST['nomDiplome']);
                  $data['confirmation'] = "Opération effectuée avec succès";
              } catch(Exception $e){
                  $data['erreur'] = 'Echec de l\'opération';
              }

              $data['diplome'] = $this->Diplomes_model->getcodeDiplomesAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerDiplome', $data);
              $this->load->view('footer');
          } else {
              $this->load->model('Diplomes_model');

              $data['diplome'] = $this->Diplomes_model->getcodeDiplomesAndName();

              $this->load->view('header');
              $this->load->view('pages/supprimerDiplome', $data);
              $this->load->view('footer');
          }
      }

    public function allDiplome(){
          $this->load->model('Diplomes_model');

          $data['diplome'] = $this->Diplomes_model->getAllDiplomes();

          $this->load->view('header');
          $this->load->view('pages/listeDiplomes', $data);
          $this->load->view('footer');

    }

    public function modifierDiplome(){
        // si on a envoyé un ordre de modification
        if(isset($_POST['nomDiplome'])){
            $this->load->model('Diplomes_model');

            try{
                $this->Diplomes_model->modifierDiplome($_POST['code'], $_POST['nomDiplome'], $_POST['niveauDiplome'], $_POST['codeU']);
                $data['confirmation'] = "Le diplome a bien été modifié";
            } catch(Exception $e){
               $data['erreur'] = "Echec lors de la modification"; 
            }

            $data['diplome'] =  $this->Diplomes_model->getAllDiplomes();

            $this->load->view('header');
            $this->load->view('pages/listeDiplomes', $data);
            $this->load->view('footer');            
        } else {
            $this->load->model('Diplomes_model');

            $data['modifier'] = $this->Diplomes_model->getDiplomeByCode($_POST['codeDiplome']);
            $data['univ'] = $this->Diplomes_model->getUnivDetails();

            $this->load->view('header');
            $this->load->view('pages/modifierDiplome', $data);
            $this->load->view('footer');
        }
    }

    public function coursParDiplome(){
          if(isset($_POST['codeDiplome'])){
              $this->load->model('Diplomes_model');

              try{
                  $data['cours'] = $this->Diplomes_model->coursParDiplome($_POST['codeDiplome']);
                  $data['confirmation'] = "Affichage des cours avec succes";
              } catch(Exception $e){
                  $data['erreur'] = "Erreur lors de la recuperation des infos";
              }
              $data['diplomeVoulu'] = $_POST['codeDiplome'];
              $data['diplomes'] = $this->Diplomes_model->getAllDiplomes();

              $this->load->view('header');
              $this->load->view('pages/coursParDiplome', $data);
              $this->load->view('footer');
          } else {
              $this->load->model('Diplomes_model');

              $data['diplomes'] = $this->Diplomes_model->getAllDiplomes();

              $this->load->view('header');
              $this->load->view('pages/coursParDiplome', $data);
              $this->load->view('footer');
          }
    }

    public function diplomesParUniv(){
        if(isset($_POST['codeUniv'])){
            $this->load->model('Diplomes_model');

            try{
                $data['diplomes'] = $this->Diplomes_model->diplomesParUniv($_POST['codeUniv']);
                $data['confirmation'] = "Affichage des diplomes avec succes";
            } catch(Exception $e){
                $data['erreur'] = "Erreur lors de la recuperation des infos";
            }

            $data['univVoulu'] = $_POST['codeUniv'];
            $data['univ'] = $this->Diplomes_model->getUnivDetails();

            $this->load->view('header');
            $this->load->view('pages/diplomesParUniv', $data);
            $this->load->view('footer');
        } else {
            $this->load->model('Diplomes_model');

            $data['univ'] = $this->Diplomes_model->getUnivDetails();

            $this->load->view('header');
            $this->load->view('pages/diplomesParUniv', $data);
            $this->load->view('footer');
        }
    }

}

?>
