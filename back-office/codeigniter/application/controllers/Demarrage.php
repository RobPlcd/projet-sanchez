<?php 

class Demarrage extends CI_Controller{

	public function accueil($page='home'){
		if(!file_exists(APPPATH.'views/pages/'. $page .'.php')){
			// La page n'existe pas
			show_404(); // fonction CI
		}

		$this->load->model("Demarrage_model");

		$data['nombres'] = $this->Demarrage_model->getNumbers();

		$this->load->view('header');
		$this->load->view('pages/'. $page, $data);
		$this->load->view('footer');
	}

}

?>