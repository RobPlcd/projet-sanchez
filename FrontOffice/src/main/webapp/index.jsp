
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Connexion</title>

    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Argon CSS -->
    <link type="text/css" href="assets/css/argon.min.css" rel="stylesheet">

    <!-- Notre style -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body class="bg-default">
 
    <!-- LA PARTIE POUR LES NOTIFICATIONS D'ERREURS -->
    <%
       String message = request.getParameter("message");
       
       if(message != null){
           out.println("<div class='notif alert alert-danger' role='alert'>"+ message +"</div>");
       }
    %>
    
    <!-- LA PARTIE POUR LES NOTIFICATIONS GOOD -->
    <%
       String good = request.getParameter("good");
       
       if(good != null){
           out.println("<div class='notif alert alert-success' role='alert'>"+ good +"</div>");
       }
    %>


    
    <div class="main-content">
        <div class="header bg-gradient-primary">
            <div class="container">
                <div class="header-body text-center py-7 py-lg-8 pt-lg-9">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white">Gestion de la mobilité étudiante</h1>
                            <p class="text-lead text-white-50">Plateforme de gestion</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-secondary border-0">
                    <div class="card-header bg-transparent">
                        <div class="text-muted text-center mt-2">
                            <h2>Connexion</h2>
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        <form role="form" action="RouteController" method="POST">
                            
                            <input type="hidden" value="start" name="controller">
                            <input type="hidden" value="authenticate" name="action">
                            <input type="hidden" value="index" name="currentPage">
                            
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="e-Mail étudiant" type="email" name="identifiant" required>
                                </div>
                            </div>
                            <div class="text-center pb-4">
                                <input type="submit" class="btn btn-primary mt-4" value="Connexion">
                            </div>
                            <div class="text-center border-top pt-4">
                                <small><a href="RouteController?controller=start&action=afficherInscription&currentPage=index" class="text-gray">Je ne possède pas de compte</a></small>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Core -->
    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Argon JS -->
    <script src="assets/js/argon.min.js"></script>
</body>
</html>