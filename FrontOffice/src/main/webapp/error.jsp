<%-- 
    Document   : error.jsp
    Created on : 18 mars 2020, 23:21:49
    Author     : thomaslamothe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <!-- Favicon -->
        <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

        <!-- Icons -->
        <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

        <!-- Argon CSS -->
        <link type="text/css" href="assets/css/argon.min.css" rel="stylesheet">

        <!-- Notre style -->
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body class="bg-gradient-danger pl-5 pt-5">
        <h1 class="text-white">Il semblerait que nous ayons une erreur :</h1>
        
        <%
          String message = request.getParameter("message");
          
          if(message != null){
              out.print("<p class='text-white'>" + message + "</p>");
          }
        %>
    </body>
</html>
