<%@page import="java.util.ArrayList"%>
<%@page import="Models.Diplomes"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inscription</title>

    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Argon CSS -->
    <link type="text/css" href="assets/css/argon.min.css" rel="stylesheet">

    <!-- Notre style -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body class="bg-default">

    <!-- LA PARTIE POUR LES NOTIFICATIONS D'ERREURS -->
    <%
       String message = request.getParameter("message");
       
       if(message != null){
           out.println("<div class='notif alert alert-danger' role='alert'>"+ message +"</div>");
       }
    %>    
    
    <div class="main-content">
        <div class="header bg-gradient-primary">
            <div class="container">
                <div class="header-body text-center py-7 py-lg-8 pt-lg-9">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white">Gestion de la mobilit� �tudiante</h1>
                            <p class="text-lead text-white-50">Plateforme de gestion</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-secondary border-0">
                    <div class="card-header bg-transparent">
                        <div class="text-muted text-center mt-2">
                            <h2>Inscription</h2>
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        <form role="form" action="RouteController" method="POST">
                            
                            <input type="hidden" value="start" name="controller">
                            <input type="hidden" value="register" name="action">
                            <input type="hidden" value="signup" name="currentPage">


                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Num�ro �tudiant" type="text" name="numEtudiant" required>
                                </div>
                            </div>
                                                        
                            
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Pr�nom �tudiant" type="text" name="prenom" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Nom �tudiant" type="text" name="nom" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="e-Mail �tudiant" type="text" name="mail" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-air-baloon"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Ann�e d'�tudes" type="number" name="annee" required>
                                </div>
                            </div>
                            
                            <!-- Le SELECT des diplomes -->
                            
                            <div class="form-group">
                                
                                    <%
                                        ArrayList<Diplomes> alldiplomes = (ArrayList<Diplomes>)(request.getAttribute("diplomes"));
                                        
                                        if(alldiplomes != null && alldiplomes.size() > 0){
                                             out.print("<select name=\"codeDiplome\">");
                                             for(int i = 0; i < alldiplomes.size(); i++){
                                                out.print("<option value='"+ alldiplomes.get(i).getCodeDiplome() +"'>"+ alldiplomes.get(i).getNomDiplome() +"</option>");
                                             }
                                             out.print("</select>");
                                        }

                                    %>
                            </div>                            
                            
                            <div class="text-center pb-4">
                                <input type="submit" class="btn btn-primary mt-4" value="M'inscrire">
                            </div>
                            
                            <div class="text-center border-top pt-4">
                                <small><a href="index.jsp" class="text-gray">Je poss�de d�j� un compte</a></small>
                            </div>
                            
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Core -->
    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Argon JS -->
    <script src="assets/js/argon.min.js"></script>
</body>
</html>