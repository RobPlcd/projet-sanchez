<%@page import="Models.Etudiants"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Votre profil</title>

    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Argon CSS -->
    <link type="text/css" href="assets/css/argon.min.css" rel="stylesheet">

    <!-- Notre style -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body class="g-sidenav-show g-sidenav-pinned">

    <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white">
        <div class="scroll-wrapper scrollbar-inner">
            <div class="scrollbar-inner scroll-content scroll-scrolly_visible" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 593px;">
                <div class="sidenav-header align-items-center">
                    <h1 class="navbar-brand ">Dashboard</h1>
                </div>
                <div class="navbar-inner">
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="RouteController?controller=start&action=show" class="nav-link">
                                    <i class="ni ni-bullet-list-67 text-primary"></i>
                                    <span class="nav-link-text font-weight-bold">Liste des programmes</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="RouteController?controller=demande&action=showDemandes" class="nav-link">
                                    <i class="ni ni-collection text-primary"></i>
                                    <span class="nav-link-text">Mes Demandes</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="RouteController?controller=contrat&action=showContrat" class="nav-link">                                   
                                    <i class="ni ni-paper-diploma text-primary"></i>
                                    <span class="nav-link-text">Mes contrats</span>
                                </a>
                            </li>
                        </ul>
                        <hr class="my-3">
                    </div>
                </div>
            </div>
        </div>
    </nav>
    
    <div class="main-content">
        <nav class="navbar navbar-expand-lg navbar-dark bg-gradient-primary">
            <div class="container-fluid">

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle">
                                        <img src="assets/img/brand/undraw_female_avatar_w3jk.svg" alt="">
                                    </span>
                                    <span class="media-body ml-2 d-none d-lg-block">
                                        <%
                                          if(session.getAttribute("etudiant") != null){
                                              Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                                              out.print("<span class=\"mb-0 text-sm font-weight-bold\">"+ e.getPrenomEtudiant() +" "+ e.getNomEtudiant() +"</span>");
                                          }  
                                        %>
                                    </span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                                <a class="dropdown-item" href="RouteController?controller=demande&action=showDemandes">Mes demandes</a>
                                <a class="dropdown-item" href="RouteController?controller=contrat&action=showContrat">Mes contrats</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="RouteController?controller=start&action=disconnect&currentPage=Views/home">Déconnexion</a>
                            </div>
                        </li>
                    </ul>

            </div>
        </nav>  
                                    
                                    
        <div class="container mt-5">