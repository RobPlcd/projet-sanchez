<%@page import="Models.Contrat"%>
<%@page import="java.util.ArrayList"%>
<%@include file="left.jsp" %>

    <!-- LA PARTIE POUR LES NOTIFICATIONS GOOD -->
    <%
       String good = request.getParameter("good");
       
       if(good != null){
           out.println("<div class='notif alert alert-success' role='alert'>"+ good +"</div>");
       }
    %>
    
    <div class="title">
        <h1>Tous mes contrats</h1>
    </div>
    <div class="table-responsive mt-4">
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Dur�e du contrat</th>
                    <th scope="col">Demande mobilit� associ�e</th>
                    <th scope="col">Etat</th>
                    <th scope="col">Mettre � jour</th>
                    <th scope="col">Demande</th>
                </tr>
            </thead>
            <tbody class="list">
                <%
                    ArrayList<Contrat> contrats = (ArrayList<Contrat>)(request.getAttribute("contrats"));
                    
                    if(contrats != null){
                        for(Contrat c : contrats){
                            out.print("<tr><th scope='row'>"+ c.getCodeContrat() +"</th><td>"+ c.getDureeContrat() +"</td><td>"+ c.getCodeDemandeM() +"</td>");
                            
                            // Gestion des �tats du contrat
                            if(c.getEtatContrat().equals("� r�aliser")){
                                out.print("<td><span class='badge badge-info'>� r�aliser</span></td><td><a class='btn-sm btn-info' href='RouteController?controller=contrat&action=passerCours&contratConcerne="+ c.getCodeContrat() +"'>En cours</a></td>");
                                
                                // Si une demande n'a pas d�j� �t� faite
                                if(c.getDemandeF() == null){
                                    out.print("<td><a href='RouteController?controller=demande&action=demandeBourse&contratConcerne="+ c.getCodeContrat() +"' class='btn-sm btn-primary'>Demander une bourse</a></td>");
                                } else {
                                    out.print("<td></td>");
                                }

                            } else if(c.getEtatContrat().equals("en cours")){
                                out.print("<td><span class='badge badge-primary'>En cours</span></td><td><a class='btn-sm btn-info' href='RouteController?controller=contrat&action=passerRealise&contratConcerne="+ c.getCodeContrat() +"'>R�alis�</button></a><td></td>");
                            } else {
                                out.print("<td><span class='badge badge-warning'>r�alis�</span></td><td></td><td></td>");
                            }
                            out.println("</tr>");
                        }
                    }
                %>
            </tbody>
        </table>
    </div>

<%@include file="bottom.jsp"%>