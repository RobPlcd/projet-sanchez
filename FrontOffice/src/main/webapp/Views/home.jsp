<%@page import="Models.Cours"%>
<%@page import="Models.Programmes"%>
<%@page import="java.util.ArrayList"%>
<%@include file="left.jsp" %>

    <!-- LA PARTIE POUR LES NOTIFICATIONS D'ERREURS -->
    <%
       String message = request.getParameter("message");
       
       if(message != null){
           out.println("<div class='notif alert alert-danger' role='alert'>"+ message +"</div>");
       }
    %>
    
    <div class="title">
        <h1>Liste des Programmes <small>(Selon vos �tudes)</small></h1>
    </div>
    <div class="table-responsive mt-4">
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Nom du Programme</th>
                    <th scope="col">Dur�e (en jours)</th>
                    <th scope="col">Dipl�me associ�</th>
                    <th scope="col">Cours</th>
                    <th scope="col">Postuler</th>
                </tr>
            </thead>
            <tbody class="list">
                <%
                  ArrayList<Programmes> programmes = (ArrayList<Programmes>)(request.getAttribute("programmes"));
                  
                  if(programmes != null || !programmes.isEmpty()){
                      // Affichage des programmes personnalis�s en fonction du diplomes
                      for(Programmes p : programmes){
                          out.print("<tr><th scope='row'>"+ p.getNomProgramme() +"</th><td>"+ p.getDureeEchange() +"</td><td>"+ p.getDiplome1().getNomDiplome() +"</td><td>");
                          
                          // Affichage des cours
                          for(Cours c : p.getDiplome1().getCours()){
                              out.print(c.getLibelleCours() + ", ");
                          }
                          out.print("...</td><td><a class=\"btn btn-primary btn-sm\" href='RouteController?controller=demande&action=create&codeProgramme="+ p.getCodeProgramme() +"&codeDiplome1="+ p.getCodeDiplome_1() +"'>Postuler</a></td></tr>");
                      }
                  } else {
                      out.print("<tr><th scope='row'>Pas de programmes concernant vos �tudes</th><td></td><td></td><td></td></tr>");
                  }
                %>
            </tbody>
        </table>
    </div>

<%@include file="bottom.jsp" %>