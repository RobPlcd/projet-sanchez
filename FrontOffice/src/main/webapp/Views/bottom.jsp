        </div>
    </div>

    <!-- Core -->
    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Argon JS -->
    <script src="assets/js/argon.min.js"></script>

    <!-- Pour trier le tableau -->
    <script src="assets/vendor/list.js/dist/list.min.js"></script>

</body>
</html>