<%@page import="Models.DemandeFinancement"%>
<%@page import="Models.DemandeMobilite"%>
<%@page import="Models.Programmes"%>
<%@page import="java.util.ArrayList"%>
<%@include file="left.jsp" %>

    <!-- LA PARTIE POUR LES NOTIFICATIONS GOOD -->
    <%
       String good = request.getParameter("good");
       
       if(good != null){
           out.println("<div class='notif alert alert-success' role='alert'>"+ good +"</div>");
       }
    %>

    <div class="title">
        <h1>Toutes mes demandes</h1>
    </div>
    <h3 class="mt-4">Demandes de mobilit�</h3>

    <div class="table-responsive mt-4">
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date demande</th>
                    <th scope="col">Programme associ�</th>
                    <th scope="col">Etat de la demande</th>
                    <th scope="col">Valider</th>
                </tr>
            </thead>
            <tbody class="list">
                <%
                  ArrayList<DemandeMobilite> demandes = (ArrayList<DemandeMobilite>)(request.getAttribute("demandesM"));
                  
                  if(demandes != null || !demandes.isEmpty()){
                      // Affichage des programmes personnalis�s en fonction du diplomes
                      for(DemandeMobilite d : demandes){
                          out.print("<tr><th scope='row'>"+ d.getCodeDemandeM() +"</th><td>"+ d.getDateDepotDemandeM().toString() +"</td><td>"+ d.getProgrammeAssocie().getNomProgramme() +"</td><td>");
                          
                          // EN fonction de la demande
                          if(d.getEtatDemandeM().equals("attente")){
                              out.print("<span class=\"badge badge-info\">en attente</span></td><td>");
                          } else if(d.getEtatDemandeM().equals("accept�e")){
                              out.println("<span class=\"badge badge-success\">accept�e</span></td>");
                              
                              if(d.getContratCree() == null){
                                    out.print("<td><a class='btn-sm btn-success' href='RouteController?controller=contrat&action=generate&codeDemande="+ d.getCodeDemandeM() +"'>Valider</a></td>");   
                              } else {
                                  out.print("<td></td>");
                              }
                          } else {
                              out.print("<span class=\"badge badge-danger\">refus�e</span></td><td>");
                          }
                          
                          out.println("</td></tr>");
                      }
                  } else {
                      out.print("<tr><th scope='row'>Pas de programmes concernant vos �tudes</th><td></td><td></td><td></td></tr>");
                  }
                %>
            </tbody>
        </table>
    </div>    
    
    <h3 class="mt-4">Demandes de bourses</h3>
    
    <div class="table-responsive mt-4" data-toggle="list" data-list-values='["name", "date", "name_2", "name_3"]'>
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th scope="col" class="sort" data-sort="name">#</th>
                    <th scope="col" class="sort" data-sort="date">Date demande</th>
                    <th scope="col" class="sort" data-sort="name_3">Contrat associ�</th>
                    <th scope="col">Montant allou�</th>
                    <th scope="col" class="sort" data-sort="name_2">Etat de la demande</th>
                </tr>
            </thead>
            <tbody class="list">
                <%
                  ArrayList<DemandeFinancement> demandesF = (ArrayList<DemandeFinancement>)(request.getAttribute("demandesF"));
                  
                  if(demandesF != null || !demandesF.isEmpty()){
                      // Affichage des programmes personnalis�s en fonction du diplomes
                      for(DemandeFinancement d : demandesF){
                         out.print("<tr><th scope='row'>"+ d.getCodeDemandeF() +"</th><td>"+ d.getDateDepotDemandeF().toString() +"</td><td>"+ d.getCodeContrat() +"</td><td>"+ d.getMontantDemandeF() +"</td>");
                          
                         if(d.getEtatDemandeF().equals("attente")){
                             out.print("<td><span class='badge badge-info'>En attente</span></td>");
                         } else if(d.getEtatDemandeF().equals("accept�e")){
                             out.print("<td><span class='badge badge-success'>Accept�e</span></td>");
                         } else {
                             out.print("<td><span class='badge badge-danger'>Refus�e</span></td>");
                         }
                          
                         out.print("</tr>");
                      }
                  } else {
                      out.print("<tr><th scope='row'>Pas de programmes concernant vos �tudes</th><td></td><td></td><td></td></tr>");
                  }
                %>
            </tbody>
        </table>
    </div>
                
<%@include file="bottom.jsp"%>