<%@page import="Models.DemandeMobilite"%>
<%@page import="Models.Cours"%>
<%@page import="Models.Programmes"%>
<%@page import="java.util.ArrayList"%>
<%@include file="left.jsp" %>

    <!-- LA PARTIE POUR LES NOTIFICATIONS D'ERREURS -->
    <%
       String message = request.getParameter("message");
       
       if(message != null){
           out.println("<div class='notif alert alert-danger' role='alert'>"+ message +"</div>");
       }
    %>
    
    <div class="title">
        <h1>S�lection des cours</h1>
    </div>
    <div class="table-responsive mt-4">
        <form action="RouteController" method="POST">

        <input type="hidden" name="controller" value="demande">
        <input type="hidden" name="action" value="ajouterCours">
        <%
            DemandeMobilite codeMobilite = (DemandeMobilite)request.getAttribute("codeMobilite");
            
            out.print("<input type=\"hidden\" name=\"codeMobilite\" value=\""+ codeMobilite.getCodeDemandeM() +"\">");
        %>
        
            
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th scope="col">S�lection</th>
                    <th scope="col">Nom du cours</th>
                    <th scope="col">Nombre d'ECTS</th>
                    <th scope="col">Ann�e</th>
                </tr>
            </thead>
            <tbody class="list">
                <%
                  ArrayList<Cours> cours = (ArrayList<Cours>)(request.getAttribute("coursUnivAccueil"));
                  
                  for(Cours c : cours){
                      out.print("<tr><th scope='row'><input type='checkbox' value='"+ c.getCodeCours() +"' name='lesCours'></th><td>"+ c.getLibelleCours() +"</td><td>"+ c.getNbECTS() +"</td><td>"+ c.getAnnee() +"</td></tr>");
                  }
                %>
            </tbody>
        </table>
            
        <input type="submit" class="btn btn-primary" value="Valider ma s�lection de cours">    
        </form>
    </div>

<%@include file="bottom.jsp" %>