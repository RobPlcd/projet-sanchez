/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author thomaslamothe
 */
public class RouteController extends HttpServlet{
    
    // La liste de tous nos controlleurs
    private String[] controllers = {"start", "contrat", "demande"};
    
    //Fonction pour tester la validité du controleur demandé
    public boolean isValid(String ctrl){
        int i=0;
        while(i<this.controllers.length){
            if (this.controllers[i].equals(ctrl)){
                return true;
            }
            i++;
        }
        return false;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        // Les paramètres en POST
        String controller = request.getParameter("controller");
        String currentPage = request.getParameter("currentPage");
        
        //Request dispatcher pour faire le routage
        RequestDispatcher rd = null;
        
        // Le Routage se fait via le switch évaluant les différents Controlleurs
        if (isValid(controller)){
            switch(controller){
                case "start":{
                    rd= request.getRequestDispatcher("StartController");
                }
                break;
                
                case "contrat": {
                    rd= request.getRequestDispatcher("ContratController");
                }
                break;
                
                case "demande": {
                    rd= request.getRequestDispatcher("DemandeController");
                    
                }
                break;
                
                default:{
                    rd = request.getRequestDispatcher(currentPage + ".jsp?message=Controleur existant mais non defini");
                }
                break;
            }
        }
        else{
            rd = request.getRequestDispatcher("error.jsp?message=Le controleur n'existe pas");
        }
        
        // Transmettre les données
        // TRES IMPORTANT
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
