/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Contrat;
import Models.Etudiants;
import Models.Diplomes;
import Models.Programmes;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ContratController extends HttpServlet {

    // Liste des actions disponibles sur ce Controlleur
    private String[] actions = {"showContrat", "generate", "passerCours", "passerRealise"};
    
    
    public boolean isValid(String action){
        int i=0;
        while(i<this.actions.length){
            if (this.actions[i].equals(action)){
                return true;
            }
            i++;
        }
        return false;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        //Récupérer le nom de l'action cible
        String action = request.getParameter("action");
        
        //Récupérer la session en cours
        HttpSession session = request.getSession(false);
                

        //Request dispatcher pour faire le routage
        RequestDispatcher rd = null;
        
        //Mettre en place le routage principal
        if (isValid(action)){
            switch(action){
                case "showContrat": {
                    // On récupère l'étudiant
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    ArrayList<Contrat> contrats = Contrat.getAllContratsByNumEtu(e.getNumEtudiant());
                    request.setAttribute("contrats", contrats);                    
                    
                    // Afficher la page voulue
                    rd = request.getRequestDispatcher("Views/listeContrat.jsp");
                }
                break;
                case "generate": {
                    // Permet de générer un contrat pour une demande de mobilité
                    // On part du postulat que la durée du contrat a la même durée que la durée de l'échange
                    int codeDemande = Integer.parseInt(request.getParameter("codeDemande"));
                    
                    // On génère le contrat
                    Contrat.genererContrat(codeDemande);
                    
                    // On récupère l'étudiant
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    // Affiche que le contrat à bien été généré
                    ArrayList<Contrat> contrats = Contrat.getAllContratsByNumEtu(e.getNumEtudiant());
                    request.setAttribute("contrats", contrats);
                    
                    rd = request.getRequestDispatcher("Views/listeContrat.jsp?good=Le contrat a bien été généré");
                    
                }
                break;
                case "passerCours": {
                    // On récupère le code du contrat
                    int codeContrat = Integer.parseInt(request.getParameter("contratConcerne"));
                    
                    Contrat.mettreAJourStatus(codeContrat, "en cours");
                    
                    // On récupère l'étudiant
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    ArrayList<Contrat> contrats = Contrat.getAllContratsByNumEtu(e.getNumEtudiant());
                    request.setAttribute("contrats", contrats);                    
                    
                    // Renvoie vers la page des contrats
                    rd = request.getRequestDispatcher("Views/listeContrat.jsp?good=Contrat mis à jour avec succès");
                    
                }
                break;
                case "passerRealise": {
                    // On récupère le code du contrat
                    int codeContrat = Integer.parseInt(request.getParameter("contratConcerne"));
                    
                    Contrat.mettreAJourStatus(codeContrat, "réalisé");
                    
                    // On récupère l'étudiant
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    ArrayList<Contrat> contrats = Contrat.getAllContratsByNumEtu(e.getNumEtudiant());
                    request.setAttribute("contrats", contrats);                    
                    
                    // Renvoie vers la page des contrats
                    rd = request.getRequestDispatcher("Views/listeContrat.jsp?good=Contrat mis à jour avec succès");
                                 
                }
                break;
                default:{
                    rd = request.getRequestDispatcher("error.jsp?message=action existante mais non definie");
                }
                break;
            }
        }
        else{
            rd = request.getRequestDispatcher("error.jsp?message=L'action demandée n'existe pas");
        }
        //Faire suivre la requete vers le dispatcher
        rd.forward(request, response);
        
    }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>        
}
