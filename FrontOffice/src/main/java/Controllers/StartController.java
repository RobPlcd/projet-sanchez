package Controllers;

import Models.Etudiants;
import Models.Diplomes;
import Models.Programmes;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class StartController extends HttpServlet {
    
    // Liste des actions disponibles sur ce Controlleur
    private String[] actions = {"authenticate", "afficherInscription", "register", "disconnect", "show"};
    
    
    public boolean isValid(String action){
        int i=0;
        while(i<this.actions.length){
            if (this.actions[i].equals(action)){
                return true;
            }
            i++;
        }
        return false;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        //Récupérer le nom de l'action cible
        String action = request.getParameter("action");
        
        //Récupérer la session en cours
        HttpSession session = request.getSession(false);
        
        // Recuperer la page d'ou nous venons
        String currentPage = request.getParameter("currentPage");
        
        //Request dispatcher pour faire le routage
        RequestDispatcher rd = null;
        
        //Mettre en place le routage principal
        if (isValid(action)){
            switch(action){
                case "show": {
                    Etudiants user = (Etudiants)(session.getAttribute("etudiant"));
                    
                    ArrayList<Programmes> programmesPerso = (ArrayList<Programmes>)(Programmes.getProgrammesPerso(user.getCodeDiplome()));

                    request.setAttribute("programmes", programmesPerso);

                    rd = request.getRequestDispatcher("Views/home.jsp");                    
                }
                break;
                case "authenticate":{
                    String login = request.getParameter("identifiant");
                    
                    Etudiants user = Etudiants.getEtudiantWithMail(login);
                    
                    if (user != null){
                        session.setAttribute("etudiant", user);
                        
                        ArrayList<Programmes> programmesPerso = (ArrayList<Programmes>)(Programmes.getProgrammesPerso(user.getCodeDiplome()));
                        
                        request.setAttribute("programmes", programmesPerso);
                        
                        rd = request.getRequestDispatcher("Views/home.jsp");
                    }
                    else{
                        rd = request.getRequestDispatcher("index.jsp?message=Utilisateur inconnu");
                    }
                }
                break;
                case "afficherInscription":{
                    // Il faut aller chercher la liste des Diplomes que l'étudiant prépare
                    ArrayList<Diplomes> diplomes = Diplomes.getAllDiplomes();
                    request.setAttribute("diplomes", diplomes);
                    
                    // Puis renvoyer sur la page d'inscription
                    rd = request.getRequestDispatcher("signup.jsp");
                }
                break;
                case "register": {
                    // Récupérer les parametres 
                    String numEtudiant = request.getParameter("numEtudiant");
                    String nom = request.getParameter("nom");
                    String prenom = request.getParameter("prenom");
                    String mail = request.getParameter("mail");
                    int codeDiplome = Integer.parseInt(request.getParameter("codeDiplome"));
                    int annee = Integer.parseInt(request.getParameter("annee"));
                    
                    Etudiants.ajouterEtudiant(numEtudiant, nom, prenom, mail, annee, codeDiplome);
                    
                    rd = request.getRequestDispatcher("index.jsp?good=Votre inscription a bien été prise en compte");
                }
                break;
                case "disconnect": {
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    if(e != null){
                        session.invalidate();
                        rd = request.getRequestDispatcher("index.jsp?good=Vous avez bien été déconnecté");
                    } else {
                        rd = request.getRequestDispatcher(currentPage + ".jsp?message=Erreur lors de la déconnexion");
                    }
                }
                break;
                default:{
                    rd = request.getRequestDispatcher(currentPage + ".jsp?message=action existante mais non definie");
                }
                break;
            }
        }
        else{
            rd = request.getRequestDispatcher(currentPage + ".jsp?message=L'action demandée n'existe pas");
        }
        //Faire suivre la requete vers le dispatcher
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>    
}
