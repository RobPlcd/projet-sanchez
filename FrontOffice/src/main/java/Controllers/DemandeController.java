/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Concerner;
import Models.Cours;
import Models.DemandeFinancement;
import Models.DemandeMobilite;
import Models.Etudiants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DemandeController extends HttpServlet {

    // Liste des actions disponibles sur ce Controlleur
    private String[] actions = {"showDemandes", "demandeBourse", "create", "ajouterCours"};
    
    
    public boolean isValid(String action){
        int i=0;
        while(i<this.actions.length){
            if (this.actions[i].equals(action)){
                return true;
            }
            i++;
        }
        return false;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        //Récupérer le nom de l'action cible
        String action = request.getParameter("action");
        
        //Request dispatcher pour faire le routage
        RequestDispatcher rd = null;
        
        //Récupérer la session en cours
        HttpSession session = request.getSession(false);
        
        //Mettre en place le routage principal
        if (isValid(action)){
            switch(action){
                case "create":{
                    // Récupération de l'étudiant via sa session
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    int codeProgramme = Integer.parseInt(request.getParameter("codeProgramme"));
                    int codeDiplomeAccueil = Integer.parseInt(request.getParameter("codeDiplome1"));
                    
                    // Creation d'une nouvelle demande
                    DemandeMobilite.createNewDemande(e.getNumEtudiant(), codeProgramme);
                    
                    // On selectionne les cours proposée par le diplome d'accueil du programme
                    ArrayList<Cours> cours = Cours.getCoursByDiplome(codeDiplomeAccueil);
                    
                    // On transmets les cours à la prochaine page pour la sélection
                    request.setAttribute("coursUnivAccueil", cours);
                    
                    request.setAttribute("codeMobilite", DemandeMobilite.getLastDemande());
                    
                    // Page de sélection des cours
                    rd = request.getRequestDispatcher("Views/select.jsp?good=Votre inscription a été prise en compte, sélectionnez les cours");
                }
                break;
                case "ajouterCours": {
                    String[] cours = request.getParameterValues("lesCours");
                    
                    if(cours != null || cours.length > 0){
                        List list = Arrays.asList(cours);
                        List<String> listString = (List<String>)list; 

                        int codeMobilite = Integer.parseInt(request.getParameter("codeMobilite"));
                        
                        listString.forEach((s) -> {
                            Concerner.ajouterCours(codeMobilite, Integer.parseInt(s));
                        });                       
                    }

                    // Récupération de l'étudiant via sa session
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    // Aller chercher les demandes de mobilité de l'étudiant
                    ArrayList<DemandeMobilite> demandes = DemandeMobilite.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesM", demandes);
                    
                    // Recuperation des demandes de financement
                    ArrayList<DemandeFinancement> demandesF = DemandeFinancement.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesF", demandesF);
                    
                    // Aller chercher les demandes de bourses de l'étudiant
                    rd = request.getRequestDispatcher("Views/listeDemandes.jsp?good=Votre inscription a été enregistré");
                   
                }
                break;
                case "showDemandes": {
                    // Récupération de l'étudiant via sa session
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));
                    
                    // Aller chercher les demandes de mobilité de l'étudiant
                    ArrayList<DemandeMobilite> demandes = DemandeMobilite.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesM", demandes);
                    
                    // Recuperation des demandes de financement
                    ArrayList<DemandeFinancement> demandesF = DemandeFinancement.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesF", demandesF);
                    
                    // Aller chercher les demandes de bourses de l'étudiant
                    rd = request.getRequestDispatcher("Views/listeDemandes.jsp");
                }
                break;
                case "demandeBourse": {
                    // Récupération de l'étudiant via sa session
                    Etudiants e = (Etudiants)(session.getAttribute("etudiant"));

                    int codeContrat = Integer.parseInt(request.getParameter("contratConcerne"));
                    
                    DemandeFinancement.createDemande(e.getNumEtudiant(), codeContrat);
                    
                    // Aller chercher les demandes de mobilité de l'étudiant
                    ArrayList<DemandeMobilite> demandes = DemandeMobilite.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesM", demandes);
                    
                    // Recuperation des demandes de financement
                    ArrayList<DemandeFinancement> demandesF = DemandeFinancement.getDemandesByStudent(e.getNumEtudiant());
                    
                    request.setAttribute("demandesF", demandesF);
                    
                    // Aller chercher les demandes de bourses de l'étudiant
                    rd = request.getRequestDispatcher("Views/listeDemandes.jsp?good=Demande de financement ajoutée avec succès");
                }
                break;
                default:{
                    rd = request.getRequestDispatcher("error.jsp?message=action existante mais non definie");
                }
                break;
            }
        }
        else{
            rd = request.getRequestDispatcher("error.jsp?message=L'action n'existe pas");
        }
        //Faire suivre la requete vers le dispatcher
        rd.forward(request, response);
        
    }    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>        
}
