package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DemandeMobilite implements Serializable {

    private int codeDemandeM;
    private Date dateDepotDemandeM;
    private String etatDemandeM;
    private String numEtudiant;
    private int codeProgramme;
    
    private Programmes programmeAssocie;
    private Contrat contratCree;

    public Contrat getContratCree() {
        return contratCree;
    }

    public void setContratCree(Contrat contratCree) {
        this.contratCree = contratCree;
    }

    public Programmes getProgrammeAssocie() {
        return programmeAssocie;
    }

    public void setProgrammeAssocie(Programmes programmeAssocie) {
        this.programmeAssocie = programmeAssocie;
    }
    
    public int getCodeDemandeM() {
        return codeDemandeM;
    }

    public void setCodeDemandeM(int codeDemandeM) {
        this.codeDemandeM = codeDemandeM;
    }

    public Date getDateDepotDemandeM() {
        return dateDepotDemandeM;
    }

    public void setDateDepotDemandeM(Date dateDepotDemandeM) {
        this.dateDepotDemandeM = dateDepotDemandeM;
    }

    public String getEtatDemandeM() {
        return etatDemandeM;
    }

    public void setEtatDemandeM(String etatDemande) {
        this.etatDemandeM = etatDemande;
    }

    public String getNumEtudiant() {
        return numEtudiant;
    }

    public void setNumEtudiant(String numEtudiant) {
        this.numEtudiant = numEtudiant;
    }

    public int getCodeProgramme() {
        return codeProgramme;
    }

    public void setCodeProgramme(int codeProgramme) {
        this.codeProgramme = codeProgramme;
    }
    
    public static ArrayList<DemandeMobilite> getDemandesByStudent(String numEtu){
        ArrayList<DemandeMobilite> demandes = new ArrayList<>();
        
        try{
            String sql = "SELECT * FROM demandesmobilite WHERE numEtudiant = '" + numEtu + "'";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
          
            while (rs.next()){
                DemandeMobilite d = new DemandeMobilite();
                d.setCodeDemandeM(rs.getInt("codeDemandeM"));
                d.setDateDepotDemandeM(rs.getDate("dateDepotDemandeM"));
                d.setEtatDemandeM(rs.getString("etatDemandeM"));
                d.setNumEtudiant(rs.getString("numEtudiant"));
                d.setCodeProgramme(rs.getInt("codeProgramme"));
                d.setProgrammeAssocie(Programmes.getProgrammeWithCode(rs.getInt("codeProgramme")));
                d.setContratCree(Contrat.getAllContratByMobilite(rs.getInt("codeDemandeM")));
                demandes.add(d);
            }

            dbutils.logout(c);            
        } catch(SQLException ex){
            Logger.getLogger(DemandeMobilite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return demandes;
    }
    
    public static int getDureeProgrammeAssocie(int codeDemande) {
        int code = 0;
        
        try{
            String sql = "SELECT dureeEchange FROM programmes, demandesmobilite WHERE programmes.codeProgramme = demandesmobilite.codeProgramme AND codeDemandeM = " + codeDemande;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
         
            if(rs.first()){
                code = rs.getInt("dureeEchange");
            }
            
            dbutils.logout(c);            
        } catch(SQLException ex){
            Logger.getLogger(DemandeMobilite.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        return code;
    }
    
    public static void createNewDemande(String numEtu, int codeProgramme){
        String sql = "INSERT INTO `demandesmobilite`(`dateDepotDemandeM`, `etatDemandeM`, `numEtudiant`, `codeProgramme`) VALUES('"+ LocalDate.now() +"', 'attente', '"+ numEtu +"', "+ codeProgramme +")";
    
        java.sql.Connection c = dbutils.connect();
        dbutils.update(sql, c);
        dbutils.logout(c);         
    }
    
    public static DemandeMobilite getLastDemande(){
        DemandeMobilite demande = null;
        
        try{
            String sql = "SELECT * FROM demandesmobilite ORDER BY codeDemandeM DESC LIMIT 0,1";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
         
            if(rs.first()){
                demande = new DemandeMobilite();
                demande.setCodeDemandeM(rs.getInt("codeDemandeM"));
                demande.setCodeProgramme(rs.getInt("codeProgramme"));
                demande.setEtatDemandeM(rs.getString("etatDemandeM"));
                demande.setDateDepotDemandeM(rs.getDate("dateDepotDemandeM"));
                demande.setNumEtudiant(rs.getString("numEtudiant"));
            }
            
            dbutils.logout(c);            
        } catch(SQLException ex){
            Logger.getLogger(DemandeMobilite.class.getName()).log(Level.SEVERE, null, ex);
        }         
        
        return demande;
    }
}
