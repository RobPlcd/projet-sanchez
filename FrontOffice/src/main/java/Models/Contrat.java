/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Contrat implements Serializable {
    private int codeContrat;
    private int dureeContrat;
    private String etatContrat;
    private int codeDemandeM;
    
    private DemandeFinancement demandeF;

    public DemandeFinancement getDemandeF() {
        return demandeF;
    }

    public void setDemandeF(DemandeFinancement demandeF) {
        this.demandeF = demandeF;
    }

    public int getCodeContrat() {
        return codeContrat;
    }

    public void setCodeContrat(int codeContrat) {
        this.codeContrat = codeContrat;
    }

    public int getDureeContrat() {
        return dureeContrat;
    }

    public void setDureeContrat(int dureeContrat) {
        this.dureeContrat = dureeContrat;
    }

    public String getEtatContrat() {
        return etatContrat;
    }

    public void setEtatContrat(String etatContrat) {
        this.etatContrat = etatContrat;
    }

    public int getCodeDemandeM() {
        return codeDemandeM;
    }

    public void setCodeDemandeM(int codeDemandeM) {
        this.codeDemandeM = codeDemandeM;
    }
    
    public static ArrayList<Contrat> getAllContratsByNumEtu(String numEtu){
        ArrayList<Contrat> contrats = new ArrayList<>();
        
        try{
            String sql = "SELECT codeContrat, dureeContrat, etatContrat, contrats.codeDemandeM "
                    + "FROM etudiants, contrats, demandesmobilite "
                    + "WHERE etudiants.numEtudiant = demandesmobilite.numEtudiant "
                    + "AND demandesmobilite.codeDemandeM = contrats.codeDemandeM "
                    + "AND etudiants.numEtudiant = '" + numEtu + "'";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);     
            
            while(rs.next()){
                Contrat con = new Contrat();
                con.setCodeContrat(rs.getInt("codeContrat"));
                con.setDureeContrat(rs.getInt("dureeContrat"));
                con.setEtatContrat(rs.getString("etatContrat"));
                con.setCodeDemandeM(rs.getInt("codeDemandeM"));
                con.setDemandeF(DemandeFinancement.getDemandeByCodeContrat(rs.getInt("codeContrat")));
                contrats.add(con);
            }
            
            dbutils.logout(c);
        } catch(SQLException ex){
            Logger.getLogger(Contrat.class.getName()).log(Level.SEVERE, null, ex);            
        }
        
        return contrats;
    }
    
    public static void genererContrat(int codeDemande){
        int duree = DemandeMobilite.getDureeProgrammeAssocie(codeDemande);
        
        String sql = "INSERT INTO `contrats`(`dureeContrat`, `etatContrat`, `codeDemandeM`) VALUES(" + duree + ", 'à réaliser', " + codeDemande + ")";
        
        java.sql.Connection c = dbutils.connect();
        dbutils.update(sql, c);
        dbutils.logout(c);        
    }
    
    public static Contrat getAllContratByMobilite(int codeDemandeM){
        Contrat contrats = null;
        
        try{
            String sql = "SELECT * FROM contrats WHERE codeDemandeM = " + codeDemandeM;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);     
            
            if(rs != null){
                if(rs.first()){
                    contrats = new Contrat();
                    contrats.setCodeContrat(rs.getInt("codeContrat"));
                    contrats.setDureeContrat(rs.getInt("dureeContrat"));
                    contrats.setEtatContrat(rs.getString("etatContrat"));
                    contrats.setCodeDemandeM(rs.getInt("codeDemandeM"));
                }                
            }
            
            dbutils.logout(c);
        } catch(SQLException ex){
            Logger.getLogger(Contrat.class.getName()).log(Level.SEVERE, null, ex);            
        }
        
        return contrats;        
    }
    
    public static void mettreAJourStatus(int codeContrat, String status){
        String sql = "UPDATE contrats SET etatContrat = '" + status + "' WHERE codeContrat = " + codeContrat;
        
        java.sql.Connection c = dbutils.connect();
        dbutils.update(sql, c);
        dbutils.logout(c);     
    }
    
}
