package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Etudiants implements Serializable {
    
    private String numEtudiant;
    private String nomEtudiant;
    private String prenomEtudiant;
    private String mailEtudiant;
    private int annee;
    private int codeDiplome;

    public String getNumEtudiant() {
        return numEtudiant;
    }

    public void setNumEtudiant(String numEtudiant) {
        this.numEtudiant = numEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    public String getMailEtudiant() {
        return mailEtudiant;
    }

    public void setMailEtudiant(String mailEtudiant) {
        this.mailEtudiant = mailEtudiant;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getCodeDiplome() {
        return codeDiplome;
    }

    public void setCodeDiplome(int codeDiplome) {
        this.codeDiplome = codeDiplome;
    }
    
    public static Etudiants getEtudiantWithMail(String mailEtu){
        Etudiants e = null;
        
        try{
            String sql = "SELECT * FROM etudiants WHERE mailEtudiant = '" + mailEtu + "'";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet result = dbutils.query(sql, c);
            
            if(result.first()){
                e = new Etudiants();
                
                e.setNumEtudiant(result.getString("numEtudiant"));
                e.setNomEtudiant(result.getString("nomEtudiant"));
                e.setPrenomEtudiant(result.getString("prenomEtudiant"));
                e.setMailEtudiant(result.getString("mailEtudiant"));
                e.setAnnee(result.getInt("annee"));
                e.setCodeDiplome(result.getInt("codeDiplome"));
            }
            
            dbutils.logout(c);
        }catch(SQLException ex){
            Logger.getLogger(Etudiants.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return e;
    }
    
    
    public static void ajouterEtudiant(String numEtudiant, String nomEtudiant, String prenomEtudiant, String mailEtudiant, int anneeEtudiant, int codeDiplome){
        String sql = "INSERT INTO `etudiants`(`numEtudiant`, `nomEtudiant`, `prenomEtudiant`, `mailEtudiant`, `annee`, `codeDiplome`) VALUES ('"+ numEtudiant +"','"+ nomEtudiant +"', '"+ prenomEtudiant +"', '"+ mailEtudiant +"', "+ anneeEtudiant +" , "+ codeDiplome +");";
    
        java.sql.Connection c = dbutils.connect();
        dbutils.update(sql, c);
        dbutils.logout(c);       
    }
}
