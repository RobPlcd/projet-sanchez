/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DemandeFinancement implements Serializable{
    
    private int codeDemandeF;
    private Date dateDepotDemandeF;
    private String etatDemandeF;
    private double montantDemandeF;
    private int codeContrat;
    private String numEtudiant;
 
    public int getCodeDemandeF() {
        return codeDemandeF;
    }

    public void setCodeDemandeF(int codeDemandeF) {
        this.codeDemandeF = codeDemandeF;
    }

    public Date getDateDepotDemandeF() {
        return dateDepotDemandeF;
    }

    public void setDateDepotDemandeF(Date dateDepotDemandeF) {
        this.dateDepotDemandeF = dateDepotDemandeF;
    }

    public String getEtatDemandeF() {
        return etatDemandeF;
    }

    public void setEtatDemandeF(String etatDemandeF) {
        this.etatDemandeF = etatDemandeF;
    }

    public double getMontantDemandeF() {
        return montantDemandeF;
    }

    public void setMontantDemandeF(double montantDemandeF) {
        this.montantDemandeF = montantDemandeF;
    }

    public int getCodeContrat() {
        return codeContrat;
    }

    public void setCodeContrat(int codeContrat) {
        this.codeContrat = codeContrat;
    }

    public String getNumEtudiant() {
        return numEtudiant;
    }

    public void setNumEtudiant(String numEtudiant) {
        this.numEtudiant = numEtudiant;
    }
    
    public static ArrayList<DemandeFinancement> getDemandesByStudent(String numEtu){
        ArrayList<DemandeFinancement> demandes = new ArrayList<>();
        
        try{
            String sql = "SELECT * FROM demandesfinancement WHERE numEtudiant = '" + numEtu + "'";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
          
            while (rs.next()){
                DemandeFinancement d = new DemandeFinancement();
                d.setCodeDemandeF(rs.getInt("codeDemandeF"));
                d.setDateDepotDemandeF(rs.getDate("dateDepotDemandeF"));
                d.setEtatDemandeF(rs.getString("etatDemandeF"));
                d.setMontantDemandeF(rs.getDouble("montantDemandeF"));
                d.setCodeContrat(rs.getInt("codeContrat"));
                d.setNumEtudiant(rs.getString("numEtudiant"));
                demandes.add(d);
            }

            dbutils.logout(c);            
        } catch(SQLException ex){
            Logger.getLogger(DemandeMobilite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return demandes;
    }
    
    public static void createDemande(String numEtu, int codeContrat){
        String sql = "INSERT INTO `demandesfinancement`(`dateDepotDemandeF`, `etatDemandeF`, `montantDemandeF`, `codeContrat`, `numEtudiant`) "
                + "VALUES('"+ LocalDate.now() +"', 'attente', 00.00, "+ codeContrat +", '"+ numEtu +"')";
        
        java.sql.Connection c = dbutils.connect();
        dbutils.update(sql, c);
        dbutils.logout(c);         
    }
    
    public static DemandeFinancement getDemandeByCodeContrat(int codeContrat){
        DemandeFinancement demande = null;
        
        try{
            String sql = "SELECT * FROM demandesfinancement WHERE codeContrat = " + codeContrat;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
         
            if(rs.first()){
                demande = new DemandeFinancement();
                demande.setCodeContrat(rs.getInt("codeContrat"));
                demande.setCodeDemandeF(rs.getInt("codeDemandeF"));
                demande.setDateDepotDemandeF(rs.getDate("dateDemandeF"));
                demande.setEtatDemandeF(rs.getString("etatDemandeF"));
                demande.setMontantDemandeF(rs.getDouble("montantDemandeF"));
                demande.setNumEtudiant(rs.getString("numEtudiant"));
            }
            
            dbutils.logout(c);            
        } catch(SQLException ex){
            Logger.getLogger(DemandeFinancement.class.getName()).log(Level.SEVERE, null, ex);
        }            
        
        return demande;
    }
}
