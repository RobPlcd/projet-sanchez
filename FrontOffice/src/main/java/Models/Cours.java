package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cours implements Serializable{
   
    private int codeCours;
    private String LibelleCours;
    private String nbECTS;
    private int annee;
    private int codeDiplome;

    public int getCodeCours() {
        return codeCours;
    }

    public void setCodeCours(int codeCours) {
        this.codeCours = codeCours;
    }

    public String getLibelleCours() {
        return LibelleCours;
    }

    public void setLibelleCours(String LibelleCours) {
        this.LibelleCours = LibelleCours;
    }

    public String getNbECTS() {
        return nbECTS;
    }

    public void setNbECTS(String nbECTS) {
        this.nbECTS = nbECTS;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getCodeDiplome() {
        return codeDiplome;
    }

    public void setCodeDiplome(int codeDiplome) {
        this.codeDiplome = codeDiplome;
    }
    
    public static ArrayList<Cours> getCoursByDiplome(int codeDiplome){
        ArrayList<Cours> cours = new ArrayList<>();
        
        try{
            String sql = "SELECT * FROM cours WHERE codeDiplome = " + codeDiplome;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);

            while(rs.next()){
                Cours co = new Cours();
                co.setCodeCours(rs.getInt("codeCours"));
                co.setLibelleCours(rs.getString("LibelleCours"));
                co.setAnnee(rs.getInt("annee"));
                co.setNbECTS(rs.getString("nbECTS"));
                co.setCodeDiplome(rs.getInt("codeDiplome"));
                cours.add(co);
            }
            
            dbutils.logout(c);
        } catch(SQLException ex){
            Logger.getLogger(Programmes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cours;
    }
   
}