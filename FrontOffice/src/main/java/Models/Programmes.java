/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Programmes implements Serializable{

    private int codeProgramme;
    private String nomProgramme;
    private int dureeEchange;
    private int codeDiplome;
    private int codeDiplome_1;
    
    private Diplomes diplome;
    private Diplomes diplome1;

    public Diplomes getDiplome() {
        return diplome;
    }

    public void setDiplome(Diplomes diplome) {
        this.diplome = diplome;
    }

    public Diplomes getDiplome1() {
        return diplome1;
    }

    public void setDiplome1(Diplomes diplome1) {
        this.diplome1 = diplome1;
    }

    public int getCodeProgramme() {
        return codeProgramme;
    }

    public void setCodeProgramme(int codeProgramme) {
        this.codeProgramme = codeProgramme;
    }

    public String getNomProgramme() {
        return nomProgramme;
    }

    public void setNomProgramme(String nomProgramme) {
        this.nomProgramme = nomProgramme;
    }

    public int getDureeEchange() {
        return dureeEchange;
    }

    public void setDureeEchange(int dureeEchange) {
        this.dureeEchange = dureeEchange;
    }

    public int getCodeDiplome() {
        return codeDiplome;
    }

    public void setCodeDiplome(int codeDiplome) {
        this.codeDiplome = codeDiplome;
    }

    public int getCodeDiplome_1() {
        return codeDiplome_1;
    }

    public void setCodeDiplome_1(int codeDiplome_1) {
        this.codeDiplome_1 = codeDiplome_1;
    }
    
    public static ArrayList<Programmes> getProgrammesPerso(int codeDiplome){
        ArrayList<Programmes> programmes = new ArrayList<>();
        
        try{
            String sql = "SELECT codeProgramme, nomProgramme, dureeEchange, programmes.codeDiplome, codeDiplome_1, diplomes.nomDiplome FROM programmes, diplomes WHERE programmes.codeDiplome = " + codeDiplome + " AND codeDiplome_1 = diplomes.codeDiplome";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet result = dbutils.query(sql, c);
            
            while(result.next()){
                Programmes p = new Programmes();
                p.setCodeProgramme(result.getInt("codeProgramme"));
                p.setNomProgramme(result.getString("nomProgramme"));
                p.setDureeEchange(result.getInt("dureeEchange"));
                p.setCodeDiplome(result.getInt("codeDiplome"));
                p.setCodeDiplome_1(result.getInt("codeDiplome_1"));
                p.setDiplome1(Diplomes.getDiplome(result.getInt("codeDiplome_1")));
                programmes.add(p);
            }
            
            dbutils.logout(c);
            
        }catch(SQLException ex){
            Logger.getLogger(Programmes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return programmes;
    }
    
    
    public static Programmes getProgrammeWithCode(int codeProgramme) {
        Programmes p = null;
        
        try{
            String sql = "SELECT * FROM programmes WHERE codeProgramme = " + codeProgramme;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet result = dbutils.query(sql, c);   
            
            if(result.first()){
                p = new Programmes();
                p.setCodeProgramme(result.getInt("codeProgramme"));
                p.setNomProgramme(result.getString("nomProgramme"));
                p.setDureeEchange(result.getInt("dureeEchange"));
                p.setCodeDiplome(result.getInt("codeDiplome"));
                p.setCodeDiplome_1(result.getInt("codeDiplome_1"));
                p.setDiplome1(Diplomes.getDiplome(result.getInt("codeDiplome_1")));
            }
            
            dbutils.logout(c);
        } catch(SQLException ex){
            Logger.getLogger(Programmes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return p;
    }
}
