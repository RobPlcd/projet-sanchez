package Models;

import Utils.dbutils;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Diplomes implements Serializable{
    private int codeDiplome;
    private String nomDiplome;
    private String niveauDiplome;
    private int codeU;
    
    private ArrayList<Cours> cours = new ArrayList<>();

    public ArrayList<Cours> getCours() {
        return cours;
    }

    public void setCours(ArrayList<Cours> cours) {
        this.cours = cours;
    }

    public int getCodeDiplome() {
        return codeDiplome;
    }

    public void setCodeDiplome(int codeDiplome) {
        this.codeDiplome = codeDiplome;
    }

    public String getNomDiplome() {
        return nomDiplome;
    }

    public void setNomDiplome(String nomDiplome) {
        this.nomDiplome = nomDiplome;
    }

    public String getNiveauDiplome() {
        return niveauDiplome;
    }

    public void setNiveauDiplome(String niveauDiplome) {
        this.niveauDiplome = niveauDiplome;
    }

    public int getCodeU() {
        return codeU;
    }

    public void setCodeU(int codeU) {
        this.codeU = codeU;
    }
    
    public static Diplomes getDiplome(int codeDiplome){
        Diplomes d = null;
        
        try{
            String sql = "SELECT * FROM diplomes WHERE codeDiplome = " + codeDiplome;
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
          
            if(rs.first()){
                d = new Diplomes();
                d.setCodeDiplome(rs.getInt("codeDiplome"));
                d.setNomDiplome(rs.getString("nomDiplome"));
                d.setNiveauDiplome(rs.getString("niveauDiplome"));
                d.setCodeU(rs.getInt("codeU"));
                d.setCours(Cours.getCoursByDiplome(rs.getInt("codeDiplome")));
                
            }

            dbutils.logout(c);            
        }catch(SQLException ex){
            Logger.getLogger(Diplomes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return d;
    }
    
    public static ArrayList<Diplomes> getAllDiplomes(){
        ArrayList<Diplomes> diplomes = new ArrayList<>();
        
        try{
            String sql = "SELECT * FROM diplomes;";
            
            java.sql.Connection c = dbutils.connect();
            java.sql.ResultSet rs = dbutils.query(sql, c);
          
            while (rs.next()){
                Diplomes d = new Diplomes();
                d.setCodeDiplome(rs.getInt("codeDiplome"));
                d.setNomDiplome(rs.getString("nomDiplome"));
                d.setNiveauDiplome(rs.getString("niveauDiplome"));
                d.setCodeU(rs.getInt("codeU"));
                diplomes.add(d);
            }

            dbutils.logout(c);
        }catch(SQLException ex){
            Logger.getLogger(Diplomes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return diplomes;
    }
}
