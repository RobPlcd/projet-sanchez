# Gestion de la mobilité chez les étudiants

## Installation

### Back-Office

Le back-office tourne en PHP avec `CodeIgniter` 

Les fichiers à configurer sont donc :

* `back-office/codeigniter/application/config/config.php` et changer :

```php
$config['base_url'] = 'http://localhost:8888/projet-sanchez/back-office/codeigniter/';
```

Par votre chemin

* `back-office/codeigniter/application/config/database.php` et changer :

```php
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'root',
	'database' => 'bookmarks',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
```

Par votre configuration

### Front-Office

Le front-office est lui codé en `JSP`, le seul fichier à configurer est celui de base de données :

* `FrontOffice/src/main/java/Utils/dbutils.java` et changer :

```java
    conn = java.sql.DriverManager.getConnection(
            "jdbc:mysql://localhost:8889/bookmarks",
            "root",
            "root"
    );
```

Par votre configuration

## En cas de problème

Merci de me contacter :

<a href="mailto:thomaslamothe@free.fr">Thomas Lamothe</a>